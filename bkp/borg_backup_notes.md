Randomly create a key (store encrypted):
```
$  head -c 48 /dev/urandom | base64 > /home/<user>/.into_borg
```

Repo creation:
```
$  borg init --encryption=repokey /path/to/repo
```
Save the config file of the repo:  
`/path/to/repo/config`  
This is important because the repo config file (in case of repokey encryption) stores also the encryption key for the repo, so if the repo is corrupted you may still be able to recover the key and data.  
Note that just having access to the repo and the config file does not give access to the data, since you also need the passphrase (setup in the paragraph above).


List available backups:
```
$  borg list [repo_path]
```
Mount a backup in a folder:
```
$  borg mount REPOSITORY_OR_ARCHIVE MOUNTPOINT [PATH...]
```
e.g.
```
$  borg mount /opt/<bkp_dir>/repo1::archive1-2019-06-25T00:45:01 /root/bkp_mnt/
```
Unmount:
```
$  borg umount /root/bkp_mnt/
```
While a repo or an archive is mounted, the repo is locked and no backup can be performed to it. So, remember to unmount it when you're done, or the automatic backup routines will fail until you unlock it.

**Restoring files and folder**  
Once a borg backup is mounted it can be browsed like a regular folder. To restore a file or folder, for example, regular bash commands like `cp` can be used.  
If you want to restore a folder exactly as it was, especially reproducing ownership, you must be root (a regular user can't create files and then assign them to somebody else, so all the restored files will be owned by the restorer user) and use the option `-p` for `cp`:
```
$  cp -r -p  /home/<usr>/temp/bkp_mnt/<certain_folder> <restore_path>
```
Another way to directly restore files and/or folders, without mounting them first (so, you need to know the precise path), is the extract command of borg:
```
$  borg extract ARCHIVE [PATH...]
```
Note: this command always restores files to `.` folder, so make sure you're in the location where you want to restore the files.  
A special consideration needs to be given to the PATH parameter, if used. The archive reproduces the same paths that is passed as parameter at creation. Since you need to pass this full path (from the root of the archive to the folder you need), this same full path will be reproduced inside . folder.


check for corruption in the chuncks:
```
#  borg check -v --repository-only REPO
```
eventually repair:
```
#  borg check -v --repository-only --repair REPO
```
then check again with the first command


check archives for corruption:
```
#  borg check -v --archives-only REPO
```
in case an archive is corrupted, delete it:
```
#  borg delete --force REPO::CORRUPT_ARCHIVE
```
then run the first command again to make sure it's ok

check the repo and one archive, verifying the data in it (-p can be added to show progress):
```
$  borg -v -p check --verify-data REPO::ARCHIVE
```


more intensive checking: run a dry-run extraction:
```
#  borg extract -v --dry-run REPO::ARCHIVE
```
