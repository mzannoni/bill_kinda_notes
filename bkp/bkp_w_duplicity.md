the backup GUI in Gnome uses duplicity

the GUI offers only basic functions

--------

list backups in a repo:
```
$ duplicity collection-status <url>
```
if the repo is in `/mnt/<user>/<remote>/backup/yoda_bill`
then the `<url>` must be specified as:
`file:///mnt/<user>/<remote>/backup/yoda_bill/`
(note the 3 '`/`', 2 for the mount specif., 1 for the path)

--------

keep all full backups, but remove the incremental parts of all full older than the last N:
```
$ duplicity remove-all-inc-of-but-n-full N <url>
```
note: that command only shows what can be done; the option --force must be added to actually delete the file:
```
$ duplicity remove-all-inc-of-but-n-full N --force <url>
```

to remove all backups (both full and incremental) older than the latest N:  
```
$ duplicity remove-all-but-n-full N --force <url>
```
