## To share a folder with a CentOS guest with qemu:

First, add the file sharing hardware in virt-manager
- driver default
- mode mapped
- source path: folder in host
- target: e.g. /shared_dir

Then, in the guest (CentOS 7) install support to mount 9p file systems:
```
# yum --enablerepo centosplus install kernel-plus
```

Reboot  
Unfortunately, the default kernel is not the plus, so make sure to boot into it with grub selection.

Info: https://plone.lucidsolutions.co.nz/linux/centos/7/install-centos-plus-kernel-kernel-plus/view

You can also make the kernel plus as default and erase the regular...

Mount the folder:
```
# mount -t 9p -o trans_virtio /shared_dir /guest_mount_dir
```
