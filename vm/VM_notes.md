# Virtual Machine converions and management commands

Convert a VirtualBox vdi to qcow2:
```
$ qemu-img convert -f vdi -O qcow2 someOS.vdi someOS.qcow2
```

If you want first to convert the vdi to an uncompressed image:
```
$ VBoxManage clonehd -format RAW someOS.vdi someOS.img
$ qemu-img convert -f raw someOS.img -O qcow2 someOS.qcow2
```

To convert the raw image back to vdi:
```
$ VBoxManage convertdd someOS.img someOS.vdi
```

## Win10 VM:
- To enable the guest auto-resize, "spice-guest-tools" needs to be installed in the guest (https://www.spice-space.org/download/windows/spice-guest-tools/spice-guest-tools-latest.exe)

For Ubuntu users: make sure the user is in the kvm and libvirt groups.  

https://dennisnotes.com/note/20180614-ubuntu-18.04-qemu-setup/

Important:  
https://www.reddit.com/r/linux/comments/asw4wk/to_all_you_virtmanager_and_spice_display_users/
that says:  
To all you virt-manager and SPICE Display users out there. Finally get your clipboard sharing, dynamic resolution and other things working.

I had this problem for years now. I've been using a Windows 10 guest and created the VM with virt-manager and once also with virtinstall. I always used SPICE display to view the guest GUI.

I always had the SPICE guest tools installed in the guest. Clipboard sharing, drag and drop to guest and dynamic resolution that adapts to the window size never worked for me. I searched for a solution to this for hours but all the answers I found only mentioned installing SPICE guest tools and it should just work.

So here is the solution:

Clipboard sharing, dynamic resolution, drag and drop

Open your virt-manager and open the machine. If it's running, shut it down. Open the hardware details of the virtual machine. Choose "Add Hardware" and choose channel. Then select "com.redhat.spice.0" (or similar) as name and "Spice agent (spicevmc)" as device type. Finish. You're done. If you didn't install the SPICE guest tools in the guest yet, do it now. If something still doesn't work and you're using Wayland on your host, try Xorg.

I really don't know why this channel isn't added by default and why I couldn't find any hints to that on the web.

Shared folder

From there you can also add another feature: A shared folder. To do this, you first have to add another channel with "org.spice-space.webdav.0" (or similar) as the name. After choosing the name you can leave the other values on default. In the guest you have to install the SPICE WebDav daemon. You can find it on the SPICE website for Windows guests. For Linux it might be in your repositories. You might have to reboot your machine. Now you can use the shared folder feature of remote-viewer, virt-viewer or Gnome Boxes.

Performance issues

I experienced pretty bad performance for my Windows 10 guest even though I allocated half of my RAM (4 of 8GB) and half of my CPU threads (4 of 8) to it. I found my error there. Shut down your host. Open the hardware details of your VM in virt-manager. Open CPU settings. You've probably allocated some of your "logical host CPUs" to your VM. I allocated 4 of 8 there. But Windows only recognized 1 core so it was really slow. Click on topology. Now you have full control. I have an i5-8250U CPU with 4 cores and 8 threads. So I allocated 1 Socket, 2 Cores and 4 threads to the VM there. Now the Windows 10 guest runs quite smooth and the rest of my system also runs fine.

Finally a KVM/QEMU/SPICE setup that is as good as Virtualbox but only using fully free software. Especially if you want to use secure boot and/or if you're using Fedora, Virtualbox produces some problems. But doesn't lack any features that I need.

I really hope this will help some of you struggling with these kinds of problems.

---

https://bgstack15.wordpress.com/2017/11/05/multiple-monitors-on-windows-guest-in-kvm/  
To convert a Win10 VM from VBox:
- install gnome-boxes, virt-manager and virt-viewer
- convert the image:
  ```
  $ qemu-img convert -f vdi -O qcow2 someOS.vdi someOS.qcow2
  ```
- import the new image in virt-manager, selecting as connection type (Hypervisor) "QEMU/KVM User session" (important that it's the one with "User session", to be seen also from gnome-boxes)
- check the setup
  - Display: type: spice-server, localhost only, keep defaults
  - Video: QXL, keep defaults
  - Network: this is a bit tricky... you should set a bridge, but we couldn't find out how yet (in "User session" connection)
- to enable auto-resize, make sure that the proper hardware channel is present: "Channel spice"; if not:
  - click on "Add Hardware"
  - Channel
  - com.redhat.spice.0
  - leave defaults and apply
  - install spice guest tools in the guest
- to enable folder share, check that a Channel spice-webdav is present in the macine details, if not:
  - click on "Add Hardware"
  - Channel
  - org.spice-space.webdav.0
  - leave defaults and apply
  - install in the guest spice webdav daemon

- to have dual monitor interface:
  - add a second simple QXL video interface in VMM (Add Hardware...)
  - in "Display Spice" make sure that the server is listening (if the Address is "Localhost only", then you can connect only through localhost)
  - make sure that the selected ports are opened on the firewall (to save some trouble, keep TLS port on auto)
  - use the remote viewer to connect to the machine (you can close the virt-manager viewer)
    e.g.
    ```
    $ remote-viewer spice://localhost:5900
    ```
    if the port is 5900
  - alternatively you can use virt-viewer:
    ```
    $ virt-viewer
    ```
    and select among the running machines
  - in View -> Displays you can then enable the other display

---

https://www.guyrutenberg.com/2018/10/25/sharing-a-folder-a-windows-guest-under-virt-manager/
To share a folder:  
- make sure in the VM hardware settings, there's a Channel device called org.spice-space.webdav.0 (select the name, you can leave the rest to default values)
- by default, virt-manager shares ~/Public; if you want to change this you can do it from the virt-viewer from Files -> Preferences
- in the guest machine (Win10) install spice-webdav (https://www.spice-space.org/download/windows/spice-webdavd/)
- then make sure that the service is running:
  - open the service manager (click Start -> type services.msc -> Enter)
  - look for the service "Spice webdav proxy" and if not running, start it
- go to (e.g. with a CLI) C:\Program Files\SPICE webdav
- execute map-drive.bat
  - the shared folder should be added as a network unit
  - if you get "System error 67 has occurred. the network name cannot be found" it means the Spice webdav proxs service is not running

---

To uninstall guest additions:  
run as root the uninstall.sh script located in /opt/VBox... in the guest  
To enable autoresize and folder share in Linux guests:  
- install:
  - qemu-guest-agent
  - spice-vdagent
  - spice-webdavd
- add hw if not there:
  - com.redhat.spice.0
  - org.spice-space.webdav.0

## UEFI fix (Ubuntu)

when Ubuntu doesn't start, then in the UEFI shell of virtualbox:

```
$ sudo mount /dev/sda1 /mnt
$ cd /mnt
$ sudo echo '\EFI\ubuntu\grubx64.efi' > startup.nsh
```
