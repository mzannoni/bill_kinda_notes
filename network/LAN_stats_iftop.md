- install `iftop` (in CentOS, from `epel` repo)
- run with root permission
  ```
  # iftop
  ```
  or (without hostname lookup)
  ```
  # iftop -n
  ```
  gives network stats on the machine by traffic direction (from which to which host)
