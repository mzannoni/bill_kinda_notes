# Edit network configuration

To edit the network configuration, edit files in the folder `/etc/sysconfig`.  

If you edit the `/etc/sysconfig/network` file, you'll affect all the network of the machine.  
If you want to edit a single connection, edit its config in `/etc/sysconfig/network-scripts/`.  

Quoting a post from https://unix.stackexchange.com/questions/193893/append-dns-suffixes-to-etc-resolv-conf

"  
Settings in the file /etc/sysconfig/network apply to all network interfaces. Since all interfaces are usually part of the same domain, it is best to place the DOMAIN or SEARCH setting in this file. In the unusual case that the system has multiple interfaces, and the search domain is only valid for one of the interfaces, put the settings into the appropriate file for that interface, such as /etc/sysconfig/network-scripts/ifcfg-eth0  
"

For example, to add a '`search`' field to the `resolv.conf` of the LAN connection on s8 (CentOS 7):  
add the line:
```
SEARCH=gigzh.lan
```
to the file `/etc/sysconfig/network-scripts/ifcfg-eno1`
'`eno1`' is the name of the network connection (in previous linux it used to be 'eth0').  
Not sure if the position of the line is meaningful, I've added it after '`DNS1`'.  

You can then restart the network with  
```
# systemctl restart network
```
