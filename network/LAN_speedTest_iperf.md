- install `iperf` (in CentOS, from `epel` repo) on both machines (e.g. m1 and m2)
- on m1 start the server:
  ```
  $ iperf -s
  ```
- on m2 start the client:
  ```
  $ iperf -c m1
  ```

---

also iperf3 can be used
