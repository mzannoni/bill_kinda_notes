- install `nethogs` (in CentOS from `epel` repo)
- run it on the machine (with root permission):
  ```
  # nethogs eth0
  ```
  gives network usage on the device by process use
