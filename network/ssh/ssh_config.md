# SSH config

## Manage different keys for different hosts

Add something like the following in `~/.ssh/config`:
```
Host a-certain.host.com
  HostName a-certain.host.com
  IdentityFile ~/.ssh/id_certain_host
```
This creates a section with options specific for the `Host`.
`HostName` needs to be the network address (e.g. the URL).
`IdentityFile` is used to specify a key different from the default one.

## Define a host alias

The same type of entry can be used to define an alias for a host.
```
Host EasyName
  HostName a-certain.host.com
```
In this way, it is enough to specify the string specified as `Host` instead of the full address, for instance:
```
$ ssh user@EasyName
```
Optionally, also the `IdentityFile` can be specified for the alias as well.

## Example of config

```
Host EasyName
  HostName a-certain.host.com
  User username
  IdentityFile ~/.ssh/id_certain_host
```

In the above config we add the same host with alias as before, with the addition of the user to be used for the connection.  
With the `User` value set like this, the command  
```
$ ssh EasyName
```
is the same as:  
```
ssh -i ~/.ssh/id_certain_host username@a-certain.host.com
