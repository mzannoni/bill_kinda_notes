# SSH Agent

## Linux

### Basic commands

Start the ssh-agent manually and add you private key:  
```
$  eval "$(ssh-agent -s)"
$  ssh-add ~/.ssh/<key_file_name>
```
`<key_file_name>` should be the name of the key (not the file ending in `.pub`).  

To verify it works:  
```
$  ssh-add -l -E md5
```
and it should list the key.  
The option `-E <type>` can be omitted, so that all keys are listed.  

Run this command to add a key to the running ssh-agent:  
```
$  ssh-add ~/.ssh/<key_file_name>
```

### Start the agent automatically

These settings, in particular, have been noted down while setting it up for Manjaro, but they should be fairly generic.

In order to start the agent automatically and make sure that only one `ssh-agent` process runs at a time, add the following to `~/.bashrc` (or `~/.zshrc`):

```
if ! pgrep -u "$USER" ssh-agent > /dev/null; then
    ssh-agent -t 1h > "$XDG_RUNTIME_DIR/ssh-agent.env"
fi
if [[ ! -f "$SSH_AUTH_SOCK" ]]; then
    source "$XDG_RUNTIME_DIR/ssh-agent.env" >/dev/null
fi
```
The above keeps the keys unlocked for 1 hour.

An alternative formulation (from [VSCode documentation](https://code.visualstudio.com/remote/advancedcontainers/sharing-git-credentials)):
```
if [ -z "$SSH_AUTH_SOCK" ]; then
   # Check for a currently running instance of the agent
   RUNNING_AGENT="`ps -ax | grep 'ssh-agent -s' | grep -v grep | wc -l | tr -d '[:space:]'`"
   if [ "$RUNNING_AGENT" = "0" ]; then
        # Launch a new instance of the agent
        ssh-agent -s &> $HOME/.ssh/ssh-agent
   fi
   eval `cat $HOME/.ssh/ssh-agent`
fi
```
Extra tip: if there are `ssh-agent` instance already running (check with `ps -ax | grep ssh-agent`), kill them and start a `bash` or `zsh` again.

### `systemd` user service (preferred method)

Another option is to use a user `systemd` service, so that the agent is started at login, even if no x-session is running.

Create the user unit file: `~/.config/systemd/user/ssh-agent.service`.
```
[Unit]
Description=SSH agent (ssh-agent)

[Service]
Type=simple
Environment=SSH_AUTH_SOCK=%t/ssh-agent.socket
ExecStart=ssh-agent -D -a $SSH_AUTH_SOCK
ExecStop=kill -15 $MAINPID

[Install]
WantedBy=default.target
```
Optionally, the line
```
Environment=DISPLAY=:0
```
can be added before `ExecStart`.

Add
```
SSH_AUTH_SOCK="${XDG_RUNTIME_DIR}/ssh-agent.socket"
```
to `~/.config/environment.d/ssh_auth_socket.conf`. This adds an environment variable assignment for the `systemd` service started by the user.

Then, add this line:
```
AddKeysToAgent  yes
```
to `~/.ssh/config` to instruct the ssh client to always add the key to the agent.

To check if the service is setup ok:
```
$ systemctl --user daemon-reload
$ systemctl --user status ssh-agent.service
$ systemctl --user enable ssh-agent.service
```
To start it before reboot:
```
$ systemctl --user start ssh-agent.service
```

Add the environment variable also to your terminal.
For `zsh`, add this line to `~/.zshenv`:
```
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR"/ssh-agent.socket
```
For `bash`, add the same line to `~/.bashrc`.

### Integration with `kwallet`

From [this forum article](https://forum.manjaro.org/t/howto-use-kwallet-as-a-login-keychain-for-storing-ssh-key-passphrases-on-kde/7088/1).  

**Note**: steps 2 and 7 seem not necessary: they were not performed and everything seems to work on Manjaro, nevertheless.  
The difference may be that now, the first time a key is needed, a window pops, asking for the passphrase.  


## Windows

To setup the agent in Windows ao that it starts at startup, open a **Administrator** PowerShell and run the following commands:
```
> Set-Service ssh-agent -StartupType Automatic
> Start-Service ssh-agent
> Get-Service ssh-agent
```

And then add the key:
```
> ssh-add $HOME/.ssh/github_rsa
```
or just simply:
```
> ssh-add
```

Windows should then add this to the running agent and unlock it at login.

Useful info found in [VSCode documentation](https://code.visualstudio.com/remote/advancedcontainers/sharing-git-credentials#_using-ssh-keys).

Then it is needed to instruct `git` to use Windows ssh client:
```
> git config --global core.sshCommand C:/Windows/System32/OpenSSH/ssh.exe
```

Useful [reference](https://gist.github.com/danieldogeanu/16c61e9b80345c5837b9e5045a701c99).

## Further resources

 * To use `keychain` to manage the keys (e.g. instead of `kwallet`) and have them unlocked at login: https://unix.stackexchange.com/questions/627102/unlock-ssh-key-on-login
 * Comprehensive doc on ArchWiki: https://wiki.archlinux.org/title/SSH_keys#SSH_agents
 * Post on Manjaro forum (useful for inspiration, but it needed adjustments): https://forum.manjaro.org/t/howto-use-kwallet-as-a-login-keychain-for-storing-ssh-key-passphrases-on-kde/7088/5

