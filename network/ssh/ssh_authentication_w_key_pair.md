# Basics

To authenticate using a private-public key pair the public key needs to be appended to the file `.ssh/authorized_keys`.  
When the `ssh` server recognizes the key of the client among those, it wont require a password but use he key to authenticate and encrypt the communication.  

# Add the key to the server

```
ssh-copy-it -i ~/.ssh/<key_id> user@host
```
The `ssh-copy-id` script copies the local key to the `authorized_keys` file in the ssh server.  

An alternative to use the `ssh-copy-id` script is to manually append the public key to the `.ssh/authorized_keys` text file on the server.  
All keys listed here can be used to connect via `ssh` using key pairs.  

To get the public key to be appended:  
```
cat ~/.ssh/id_ed25519.pub
```
