# Get details about current `ssh` connections

## Command `who`

## Command `w`

## Command `last`

* The output is huge (shows all logins and their status).
  * To look only at the still running sessions:
    ```
    last | grep still
    ```

## Command `netstat`

* Looking only for ssh connections (the command itself is used to show all connections, interfaces, routing, etc.)
  ```
  netstat | grep ssh
  ```
* To show all connections not only the established ones (so both listening and non-listening):
  ```
  netstat -a | grep ssh
  ```
* This is the best command to figure out if there are still tunnels open.

## Command `ss`
```
ss -a | grep ssh
```
shows detailed info, like `netstat`.
