## measure speed from machine 192.168.1.1 to machine 192.168.1.2

192.168.1.1 = m1, 192.168.1.2 = m2  

### start a listeing nc process on m1 port 1234: 
- `ssh` to m1, then:
- ```
  $ nc -lk 1234 > /dev/null
  ```
  the output of `nc` is sent to the void


### from machine m2, use `dd` to send 10 Mbyte of zeros (625 blocks of 16000 bytes) to the listening `nc` on m1
```
$ dd if=/dev/zero bs=16000 count=625 | nc -v m1 1234
```

the output of `dd` is sent to a `nc` process that opens a connection to the `nc` open port on m1 (with verbose output)
