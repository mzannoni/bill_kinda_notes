# Useful commands for network

Get my public IP:  
```
$ dig +short myip.opendns.com @resolver1.opendns.com
```

---

command to get network and address info  
refs:
- https://www.cyberciti.biz/faq/linux-ip-command-examples-usage-syntax/
- https://phoenixnap.com/kb/linux-ip-command-examples

get info on all network interfaces  
```
$ ip a
```
the command generally follows the form: `ip <OBJECT> <COMMAND>`

---
