# Start a Python web server

Useful, for instance, to browse files remotely from a browser window.

## Start the Python web server

```
python3 -m http.server
```
for instance from an SSH session (note: if the session closes the server is terminated).  

By default it listens to port 8000.  
To specify a different port, simply pass the port number as argument:  
```
python3 -m http.server 8080
```

By default, the server binds itself to all interfaces (same as passing `--bind 0.0.0.0`).  
To specify an address to which it binds, for instance to localhost only:  
```
python3 -m http.server --bind 127.0.0.1
```

## Connect to the server

Then open remotely in a browser:  

    http://<remote_machine>:8000

Where <remote_machine> is the URL of the remote machine where you started the Python web server.  
This would let you browse files from the directory from where the python web server was launched (folder from where the above `python...` command is executed).  
