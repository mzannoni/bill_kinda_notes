# Create GPG signature

If it doesn't exist yet.  

```
# Use this command for the default version of GPG, including
# Gpg4win on Windows, and most macOS versions:
gpg --gen-key

# Use this command for versions of GPG later than 2.1.17:
gpg --full-gen-key
```

To list secret keys associated to a certain <EMAIL>:  
```
gpg --list-secret-keys --keyid-format LONG <EMAIL>
```
The key ID is the number after the `/` character in the line starting with `sec`.  

# Configure signature for a git user

This is to configure which GPG signature is used by a certain account:  
```
git config --global user.signingkey <KEY ID>
```

# Sign commits

With the option `-S`, a commit can be signed:  
```
git commit -S -m "My commit message"
```

To sign by default each commit:  
```
git config --global commit.gpgsign true
```
