# Integrate a repo into another existing one

This could be useful, for instance, if a local git repo has been created and it needs to be included in another repo (typical case could be a remote sandbox repo).  

## Steps

### In the destination repo

In the repo that will receive the repo to be included, create a branch that will get the incoming new commits (from the repo we want to include).  

```
git checkout -b <new_remote_branch>
```

And push this new branch to the remote:  

```
git push --set-upstream origin <new_remote_branch>

```

Optionally, a new empty branch can be created.  

#### Create a new empty branch

Create a new orphan branch  
```
git switch --orphan <new_remote_branch>
```

Create an empty first commit:  
```
git commit --allow-empty -m "Initial commit on new branch"
```

Push the new branch to the remote:  
```
git push -u origin <new_remote_branch>
```

### In the source repo (the one we want to add to the existing one)

Go back to the local repo.  

Optional (not needed if adding to an empty branch in the existing repo):  
make sure the folder structure matches what you want to be in the receiving repo.  
For instance, if you want this repo to be added into a subfolder, all the relevant content, except `.git` and other git configurations, can be already moved to the desired folder structure and committed.  

Add to the local repo a remote that points to the destination repo.  
```
git remote add <remote_label> <remote_addr>
```
`<remote_label>` is typically `origin` (unless the local repo already tracks an existing remote).  
`<remote_addr>` could be for instance _git@gitlab.com:mzannoni/bill_kinda_notes.git_.  

Now fetch the remote, so you have it in the local repo:
```
git fetch
```

Now, the entire trick boils down to merging the local branch (let's call it `<local_branch>`) into the branch in the remote we created just above.  
Since the new branch to be added has a completely unrelated history, we need to set the relative flag.  

Checkout the newly created branch in the remote (from the local repo, of course):  

```
git checkout <new_remote_branch>
```

If by any chance `<new_remote_branch>` exists in multiple remote, to track one not in the default remote:
```
git checkout -b <new_remote_branch> --track <remote_label>/<new_remote_branch>
```

Now merge the local branch into it:  
```
git merge --no-ff --allow-unrelated-histories -m "initial merge of <local_branch>" <local_branch>
```

Solve eventual conflicts (frequently in `.gitignore`for instance), finish the merge and push.
