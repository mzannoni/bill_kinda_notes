# Moving commits to an existing branch
For instance, moving the last 3 commits:   
```
git checkout <existing_branch_1>
git merge <branch_to_move_commits_from>
git checkout <branch_to_move_commits_from>
git reset --hard HEAD~3  # uncommited work is lost!!! Stash if needed
git checkout <existing_branch_1>
```

This changes the repo from this:  
```
<existing_branch_1>           A1 - B1
<branch_to_move_commits_from> A2 - B2 - C - D - E
```
To this:  
```
<existing_branch_1>           A1 - B1 - C - D - E
<branch_to_move_commits_from> A2 - B2
```
It is assumed that nothing has happened to `<existing_branch_1>` after the commit `B1`. Otherwise, it may be also useful or necessary to do a `git rebase` on `<existing_branch_1>` after the merge (to do a sort of clean-up).  
Also, it is assumed the moved commit don't create conflict when "moved" (actually, when merged).  

# Moving commits to a new branch
Moving the last 3 commits from `<existing_branch1>` to a new derived branch (it is supposed we're in `<existing_branch1>` when giving the following commands):  
```
git branch <new_branch>
git checkout <existing_branch1>
git reset --hard HEAD~3
git checkout <new_branch>
```
This changes the repo from this:  
```
<existing_branch_1> A - B - C - D - E
```
To this:  
```
<existing_branch_1> A - B
                         \
<new_branch>              C - D - E
```

Alternatively, to put `<existing_branch1>` back to a specific commit, you can use the command:  
```
git reset --hard a1b2c3d4
```
specifying the hash of the commit to go back to.  
Make sure the new branch is created correctly, or some commits will be lost!
