## To add a submodule in an existing repo:  
```
  $ git submodule add <remote_repo_ref> <folder_inside_the_local_repo>
```
`folder_inside_the_local_repo` is usually a folder with the same name of the repo that becomes a submodule.  

## If a submodule is updated to a newer version with a `git pull` (you'll see it modified in the superproject `git status`), but you want to stay with the old one in the superproject, do:  
```
  $ git submodule update
```
This will pin the submodule to the specific previous commit, detaching it from the head (i.e. not part of any branch); if changes are made when the submodule is in this state, they can't be commited (at least not easily; a branch of the submodule, from the commit where it points now has to be created and then merged back again, for example).

## To clone the superproject from scratch, and populate the submodule:  
```
  $ git clone <superproject_repo>
  $ git submodule update --init
```
Before this last commands, the submodules folders are there but empty.


