# add SSH key to GitHub or GitLab

In Windows, use Git Bash. You'll have your keys generated and SSH settings in `~/.ssh`.

## Generate SSH key

Look in ~/.ssh if a key already exists.

If not, create one, e.g. if you want it with format ED25519:  
```
$ ssh-keygen -t ed25519 -C "<comment>"
```

or with RSA 2048 bits format:  
```
$ ssh-keygen -t rsa -b 2048 -C "<comment>"
```

You'll be ask a location (if not set, default to `./ssh`) and for a passphrase.

## Add to online service

Copy the content of the public key to the appropriate online form.  
Keys are in folder `~/.ssh` and usually with a name like `id_ed25519.pub` or `id_rsa.pub`. Make sure you copy the public key, with extension `.pub`.  

One way to copy in Windows with Git Bash:  
```
cat ~/.ssh/id_ed25519.pub | clip
```

One way to copy in Linux, with `xclip` installed:
```
xclip -sel clip < ~/.ssh/id_ed25519.pub
```

## Add key to agent

To have it always available in the terminal session (or you'll be asked for the passphrase at every command).  

In Windows, you may have to start the agent first:  
```
$ eval `ssh-agent -s`
```

Or if you get an error like:
```
❯ ssh-add
Could not open a connection to your authentication agent.
```
This can happen for instance when you're connected in a SSH session.
You then need to start the agent first:
```
$ eval $(ssh-agent -s)
```

Add the key to the agent:  
```
$ ssh-add
```
If multiple are present, you'll be asked for which key, unless you spcify it as argument:  
```
$ ssh-add id_ed25519.pub
```
