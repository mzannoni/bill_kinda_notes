## Revert a merge commit

```
  $ git revert -m <parent_number> <commit>
```

example:
```
  $ git revert -m 2 <commit_SHA>
```

The git revert commands, creates a new commits, that "undoes" a previous one.
Now, a merge commit comes from 2 commits, so if you want to undo it, you need to specify to which of the 2 you want to go back (in other words, git can't know which one is to be thrown away).  

Looking with git show at a merge commit:
```
  $ git show <commit_SHA>
  commit <commit_SHA>
  Merge: commit_A commit_B
  Author: bill <bill@example.email>
  Date:   Thu Oct 12 10:18:49 2017 +0200

      Merge remote-tracking branch 'origin/master'
```
Here you see the 2 parents in the line beginning with "Merge: ". They are numberd left to right starting from 1.  

A useful howto with detailed description can be found at (on a Linux machine with git installed):  
`/usr/share/doc/git/howto/revert-a-faulty-merge.html`
