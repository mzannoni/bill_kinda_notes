# git cheat sheet
## Config Tooling

```
$ git config --global user.name "<name>"
```
Sets the name attached to the commits
<br/><br/>

```
$ git config --global user.email "<email address>"
```
Sets the email address attached to the commits
<br/><br/>

```
$ git config --global color.ui auto
```
Enables helpful color for the command line output


## Create Repos
```
$ git init <repo name>
```
Creates a new local repo
<br/><br/>

```
$ git clone <url>
```
Downloads a repo


## Make Changes

```
$ git status
```
List modified and new files to be committed
<br/><br/>

```
$ git diff
```
Shows file differences not yet staged
<br/><br/>

```
$ git add [file]
```
Add file to the stage for commit
<br/><br/>

```
$ git diff --staged
```
Shows differences between stage andthe last file version
<br/><br/>

```
$ git reset [file]
```
Remove file from stage, but preserves its modifications
<br/><br/>

```
$ git commit -m "<message>"
```
Commit the stage to the version history


## Group Changes
```
$ git branch
```
List local branches
<br/><br/>

```
$ git branch <branch-name>
```
Creates a new branch
<br/><br/>

```
$ git checkout <branch name>
```
Switches to the specific branch and updates the working directory
<br/><br/>

```
$ git checkout -b <branch name>
```
Creates a new branch and switches to it, moving the changes since last commit
<br/><br/>

```
$ git push --set-upstream origin <branch name>
```
Push the new branch and set the remote as upstream (`-u` can be used instead of `--set-upstream`)
<br/><br/>

```
$ git merge <branch name>
```
Merges the specific branch history into the current one
<br/><br/>

```
$ git merge --abort
```
Abort the current merge attempt. It should cancel the merge operation in progress.  
Note that this could leave the repository in a conflicted state.
E.g. to cancel a pull that generates conflicts.  
<br/><br/>

```
$ git branch -d <branch name>
```
Deletes the specific branch
<br/><br/>

```
$ git remote update [origin] -p
```
Updates the remote branches in the local folder
<br/><br/>

```
$ git merge --no-ff -m "Merge remote-tracking branch 'branch1' into <current branch>" <branch1>
```
Merges history of 'branch1' into the current one (e.g. useful to merge back from a branch before creating a pull request to it)
<br/><br/>

```
$ git branch -m <newname>
```
Rename the current branch
<br/><br/>

```
$ git branch -m <oldbranch> <newbranch>
```
Rename `oldbranch` to `newbranch`
<br/><br/>

Then the best way to rename the remote branch, is to push a new branch and delete the previous:  
```
$ git push origin -u <newname>
$ git push origin --delete <oldname>
```

## Refactor Filenames
```
$ git rm <file>
```
Removes the file from the working directory and stages the deletion
<br/><br/>

```
$ git rm --cached <file>
```
Removes the file from version control, but preserves it locally
<br/><br/>

```
$ git mv <original file> <renamed file>
```
Changes file name and stage the renaming


## Suppress Tracking
A file called `.gitignore` suppresses accidental versioning of files and paths matching a specified pattern

```
$ git ls-files --other --ignored --exclude-standard
```
List all ignored files in current repo


## Save Fragments
```
$ git stash
```
Temporarily stores all modified tracked files
<br/><br/>

```
$ git stash pop
```
Restores the most recent stashed modifications
<br/><br/>

```
$ git stash list
```
List all stashed changes
<br/><br/>

```
$ git stash drop
```
Discards the most recent stashed modifications


## Review History
```
$ git log
```
List version history for the current branch
<br/><br/>

```
$ git log --follow <file>
```
List version history for a file (including renames)
<br/><br/>

```
$ git diff <first branch>...<second branch>
```
Shows content differences between two branches
<br/><br/>

```
$ git show <commit>
```
Outputs metadata and content changes of a commit


## Redo Commits
```
$ git reset [commit]
```
Undoes all commits after the specified one (or the last commit if no commit specified), preserving changes locally
<br/><br/>

```
$ git reset --hard [commit]
```
Discard all history and changes, going back to the specified commit (or the last commit if no commit specified)


## Sync changes
```
$ git fetch [bookmark]
```
Downloads all history [from the repo bookmark]
<br/><br/>

```
$ git merge [bookmark]/[branch]
```
Merges bookmark's branch into current local branch
<br/><br/>

```
$ git push [alias] [branch]
```
Upload all local commits to the remote
<br/><br/>

```
$ git pull
```
Downloads bookmark history and merges changes


## Tool Setup
```
$ git config --global merge.tool meld
```
Add  meld as merge tool
<br/><br/>

```
$ git config --global diff.tool meld
```
Add meld as diff tool
<br/><br/>

```
$ git difftool -d <branchA>
```
Shows the difference between `branchA` and the current branch, using directory mode
<br/><br/>

```
$ git difftool -d <branchA> <branchB>
```
Shows the differences between `branchA` and `branchB`, in directory mode (in meld `branchA` is on the left and `branchB` on the right.

Note: the files you see in meld when using difftool are not real files, apparently, but a “reconstruction” from the git database.

Note: the normal diff tool of git, if used on a directory, would show the differences file by file, one after the other, easily hanging the OS.

Note: a normal 'git diff' command will not use the difftool (meld) to show the differences, but the shell itself.
<br/><br/>

```
$ git difftool <branchA>  <branchB> <file>
```
Shows the difference in file between `branchA` and `branchB`
<br/><br/>

```
$ git difftool <branchA>:<file1>  <branchB>:<file2>
```
Shows the difference between `file1` in `branchA` and `file2` in `branchB`