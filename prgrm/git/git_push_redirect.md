## prevent git from pushing to a repo:
```
  $ git remote set-url --push origin no_push
```

this command sets the url to push to to the string `no_push` instead of the remote url; can be verified by looking into the .git/config of the repo

---
