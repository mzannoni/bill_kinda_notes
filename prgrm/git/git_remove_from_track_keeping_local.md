# Remove a file from git tracking, keeping the local changes.

1. Add the file to gitignore
2. Run `git rm –chached <file>`  
  This actually removes `file` from the repo, but keeps it unchanged locally  
3. Run `git update-index –assume-unchanged <file>`  
  This prevents git from detecting changes in `file`  
4. Run `git update-index --skip-worktree <file>`  
  Have your own independent version of `file`, don’t change the one on the repo, just change it locally  

To not remove the file from the repo and ignore local changes (e.g. to keep a config files in the repo untouched), skipping the point 2. should be the proper way.  
