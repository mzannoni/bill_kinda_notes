# Setting environment variables in a conda environment

Use the `config` interface, instead of executing arbitrary code on activation.  

Once the environment is activated:  
```
$ conda env config vars set myvar=value
```
Important: **activate the environment again.**  

When the environment is deactivated, the variable is unset.  


To unset the variable:  
```
$ conda env config vars unset my_var -n my_env
```
Where `my_env` is the environment in which to unset the variable.  

Variables set like this will be retained in the output of conda env export.  


Useful ref: https://conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#setting-environment-variables  
