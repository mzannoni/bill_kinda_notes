# Python useful commands
For Python 3

---

Run a script (e.g. filename.py) from inside the interpreter:
```
>>> exec(open("./filename.py").read())
```

---
Find help:  
`help(function)` or `help(module)`  
e.g.  
```
>>> help(time)
```
and you get the doc of the time module

---

find path to a module: 'inspect' module can be used
```
>>> import inspect
>>> import conda.cli
>>> inspect.getfile(conda.cli)
```
the last command returns the path to the `__init__.py` file
