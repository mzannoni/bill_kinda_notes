# String Formatting in Python

Notes for string formatting in Python (as of writing, ver 3.10).

Refs:
* https://docs.python.org/3/tutorial/inputoutput.html
* https://docs.python.org/3/library/string.html#formatstrings

## Formatted string literals (aka "f-strings")

Refs:
* https://docs.python.org/3/tutorial/inputoutput.html#tut-f-strings
* https://docs.python.org/3/reference/lexical_analysis.html#f-strings

Newest approach (introduced in Python 3.6)  

Print the number 3, as float, with 4 decimal digits after:  
```
>>> f'{3:.4f}'
'3.0000'
```
Quick explanation of the field `{}` used above:  
- on the left of the `:` sign it's what's to be printed in that field
- on the right, the format specifier:
  - `.4` stands for 4 decimal digits
  - `f` stands for _float_
In the following, most of the format modifier meaning can be guessed from the example.

Same, but with 40 decimal digits:  
```
>>> f'{3:.40f}'
'3.0000000000000000000000000000000000000000'
```

Print the number 3, as float, giving the printed field a total length of 10 characters, including decimal separator (of which, 4 are decimal digits):
```
>>> f"{3:10.4f}"
'    3.0000'
```

Print the number 3, as integer, giving the printed field a total length of 10, aligning to the left (filling the rest with white spaces)
```
>>> f"{3:<10d}"
'3         '
```
Here the format specifier `<` is used to specify the alignment to the left.  

Print the number 3, as integer, giving the printed field a total length of 10, aligning to the right, padding with the character `_`.
```
>>> f"{3:_>10d}"
'_________3'
```
The character just before the alignment specifier is used to specify what character to use to fill the empty space to reach the specified print length.

Print the number 3.4, as float, giving the printed field a total length of 10 characters, of which 5 decimal digits, aligning to the left, padding with the character `*`.
```
>>> f"{3.4:*<10.5f}"
'3.40000***'
```

One important feature of the _f-string_ is that it can directly expand variables with their value.  
Here are a couple of examples of formatting the `pi` value, taken from a variable.  
```
>>> f'{math.pi:>30.10f}'
'                  3.1415926536'
>>> f'{math.pi:>30.20f}'
'        3.14159265358979311600'
```

Some more examples of the use of variables in _f-strings_:  
```
>>> var1=4
>>> var2=.54
>>> var3="3.0"
>>> f'var 1: {var1}, then var 2: {var2} and finally {var3}'
'var 1: 4, then var 2: 0.54 and finally 3.0'
```

And here example of variable use, with proper field formatting:  
```
>>> f'var 1: {var1:.4f}, then var 2: {round(var2):&>10d} and finally {var3}'
'var 1: 4.0000, then var 2: &&&&&&&&&1 and finally 3.0'
```

### Additional notes

Raw string literals: `r'a string'`. The backslash character '`\`' is not treated specially.

## String object format method (`str.format()`)

Refs:
* https://docs.python.org/3/tutorial/inputoutput.html#the-string-format-method
* https://docs.python.org/3/library/stdtypes.html#str.format

Objects of type string have a method `format()` that can perform all formatting.  
Following examples are almost all to get the same as in teh examples above.  

Print the number 3, as float, with 4 decimal digits after:
```
>>> '{:.4f}'.format(3)
'3.0000'
```
Quick explanation:  
- on the right of the `:` sign are the format specifier:  
  - `.4` stands for 4 decimal digits  
  - `f` stands for _float_  
- the content to be placed in the `{}` field and formatted are the arguments of _format()_.

In the following, most of the format modifier meaning can be guessed from the example.

Same, but with 40 decimal digits:
```
>>> '{:.40f}'.format(3)
'3.0000000000000000000000000000000000000000'
```

Print the number 3 as float with a total length of 10, including the decimal separator, of which 4 decimal digits:
```
>>> '{:10.4f}'.format(3)
'    3.0000'
```

Print the number 3, as integer, giving the printed field a total length of 10, aligning to the left (filling the rest with white spaces):
```
>>> '{:<10d}'.format(3)
'3         '
```

Print the number 3 as integer with a total length of 10, aligned to the right, padding with the character `_`:
```
>>> '{:_>10d}'.format(3) 
'_________3'
```

Print the number 3.4 as float with a total length of 10, of which 5 decimal digits, aligning to the left, padding with the character `*`:
```
>>> '{:*<10.5f}'.format(3.4)
'3.40000***'
```

The _format()_ method can expand variables with their value:
```
>>> '{:>30.10f}'.format(math.pi)
'                  3.1415926536'
>>> '{:>30.20f}'.format(math.pi)
'        3.14159265358979311600'
```

More examples with variables:
```
>>> var1=4
>>> var2=.54
>>> var3="3.0"
>>> 'var 1: {:d}, then var 2: {:f} and finally {:s}'.format(var1, var2, var3)
'var 1: 4, then var 2: 0.540000 and finally 3.0'
```
Note the difference from _f-strings_: each format specifier needs to specify the type of the variable to be printed.

Examples with format specifiers:
```
>>> 'var 1: {:.4f}, then var 2: {:&>10d} and finally {:s}'.format(var1, round(var2), var3)
'var 1: 4.0000, then var 2: &&&&&&&&&1 and finally 3.0'
```

The _format()_ method allows the use of locally defined variables, for instance if one needs to print the same value more than once in a string, passing `kwargs`:
```
>>> '{loc1} and {loc2}, but {loc1} is better'.format(loc1="good", loc2="bad")
'good and bad, but good is better'
```

## Format strings using the `%` sign (also known as `printf`-style string formatting)

Ref: https://docs.python.org/3/library/stdtypes.html#printf-style-string-formatting

This is similar to the _format()_ method (formatting strings, use of variables and local variables, etc.).  
However, it has some quirks that makes this method not recommended any more.  
```
>>> '%s and %d' % ("one", 2)
'one and 2'
```

The `%` sign is placed between the string to be formatted and the values or variable to substitute in the formatting fields.  
The formatting fields are not place in `{}` fields, but simply preceded by a `%` sign.

The format specification and flags are also different.  
Following are some examples.

Print the number 4.4 as float, with a total length of 4 of which 4 decimal digits, left aligned (specified by the flag `-`).
```
>>> '%-7.4f' % (4.4)
'4.4000 '
```

A dict can be used to specify mapping keys:
```
>>> 'The %(order)s is the number %(num)d.' % {'order': "first", 'num': 1}
'The first is the number 1.'
```

## Template strings (from std lib)

Ref: https://docs.python.org/3/library/string.html#template-strings

This is safer in cases in which the format string is user provided.  
It offers less formatting control.

```
>>> from string import Template
>>> s = Template('Oh my $something!')
>>> s.substitute(something="dear")
'Oh my dear!'
>>> s.substitute(something="deer")
'Oh my deer!'
>>> s.substitute(something=f'{"deer":_>10}')
'Oh my ______deer!'
```
