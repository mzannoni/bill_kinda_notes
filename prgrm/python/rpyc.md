# RPyC

## Classic mode

start RPyC server:
```
$ rpyc_classic.py
```

connect client:
```
>>> conn = rpyc.classic.connect("localhost")
```

## Server
example in `/personal/mz/worspace/rpyc/exm_server.py`

## Client

```
>>> c = rpyc.connect.(<hostname>,<port>)
```

```
>>> c.root # the service itself
```

```
>>> c.root.get_num() # calls an exposed func on the server (no need for the "exposed_" prefix)
```

```
>>> c.root.ask_sum(4,5) # calls a func that accepts arguments and return a result
```

```
# some function are built-in, like
>>> c.root.get_service_name()
```

### async calls:
```
>>> r=rpyc.async(c.root.get_patient_num) # makes the func get_patient_num async
```

that means that when
```
>>> res=r() # res is an object that deals with the func being executed, and has attributes like

>>> res.ready
False
```

that tells if the result of the called function is already available, etc...

another attribute:
```
>>> res.value
213213

# stores the value, once available
```

