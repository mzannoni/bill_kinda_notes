# Conda useful commands

## Environment management

### Create new env
```
$ conda create --name myenv
```
### From environment file:
```
$ conda env create -f environment.yml
```

### List environments:
```
$ conda info --envs
```
  or
```
$ conda env list
```

### Disable auto-activation of base environment at terminal startup (to use system Python by default)
```
$ conda config --set auto_activate_base false
```

### Disable showing of env name in command prompt:
```
$ conda config --set changeps1 False
```

### Remove environment:
```
$ conda env remove --name myenv
```
or this also works:
```
$ conda remove --name myenv --all
```

### Add a channel (e.g. conda-forge):
```
$ conda config --add channels conda-forge
```
### Make it preferred (over defaults for instance):
```
$ conda config --set channel_priority strict
```

### Edit behavior at activation for a certain env:
For each env, any scripts in the `etc/conda/activate.d` directory will be executed post-activation (likewise `etc/conda/deactivate.d` scripts for deactivation).  
Adding this script there, will remove the "`(base)`" string at the beginning of the prompt just after activating the base env:  
e.g. create a file `remove_base_ps1.sh` containing just the line:
```
PS1="$(echo "$PS1" | sed 's/(base) //') "
```
(this works for bash)

### View previous versions of an env:
```
$ conda list --revisions
```

### Go back to one version of the env:
```
$ conda install --rev <N>
```
where N is the revision number (warning: going back to rev 0 will also remove conda)

## Autocompletion for bash:
Install `argcomplete` and `conda-bash-completion` from conda.
If the base env is not activate at shell start, add these lines to the `.bashrc`:
```
CONDA_ROOT=~/anaconda3
if [[ -r $CONDA_ROOT/etc/profile.d/bash_completion.sh ]]; then
    source $CONDA_ROOT/etc/profile.d/bash_completion.sh
fi
```

## Conda in Windows
To use conda in Git Bash for Windows the `.bashrc` needs to be edited/created.  

Note: .`bashrc` is not present right after installation; it seems that Git Bash will then complain if it finds a `.bashrc` but not a `.bash_profile`; however, it will generate a `.bash_profile` automatically, so it will be ok after a second restart of Git Bash.  

The `conda.sh` script that came with Conda installation needs to be found and added to `.bashrc`.  
Usually, it is located somewhere like:  
- `<...>/Miniconda3/etc/profile.d/conda.sh`
- `<...>/Anaconda3/etc/profile.d/conda.sh`

In my case, where Miniconda was installed system-wide: `C:\ProgramData\Miniconda3\etc\profile.d`

Navigate to this directory in Git Bash and give the command:  
```
$ echo ". '${PWD}'/conda.sh" >> ~/.bashrc
```

Then reopen Git Bash and conda should be now available.
