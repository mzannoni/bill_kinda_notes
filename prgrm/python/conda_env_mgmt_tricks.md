# Pin a package to a specific version

It is possible to prevent a package in an env from ever being updated, specifying the version it should be kept at.  
To do this, simply add the package name and version to a text file called `pinned` in the folder `conda-meta` inside an env folder.  
So, for instance, we want to keep for the env called `opencv_env`, the package `opencv` pinned at version 3.  
We create a text file called `pinned` in the folder `<anaconda>/envs/ocv_3/conda-meta` and add the line:

    opencv 3.*

(the syntax `opencv=3.*` should also work...)  

To pin other packages, create one line per package. The env solver from now on will try to solve dependencies respecting these conditions.  

# Create environment.yaml file

To exporrt the current environemnt into a file that can be then used to re-create it:  
```
conda env export --from-history > environment.yaml
```

Note that without the option `--from-history` the exported package list is not cross-platform compatible, because it would be an exact snapshot of all the current packages installed in the environment, including auto-generated dependencies. The dependencies are in general system-dependent.  
