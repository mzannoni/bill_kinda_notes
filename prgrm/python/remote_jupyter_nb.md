# Run and open Jupyter notebook on remote machine

## Start the notebook on the remote machine

```
❯ ssh bill@yoda
bill@yoda's password: 
Welcome to Ubuntu 22.04.1 LTS (GNU/Linux 5.15.0-56-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

0 updates can be applied immediately.

Last login: Sat Dec 10 14:48:38 2022 from 192.168.1.154
❯ mamba activate math_mz
❯ jupyter notebook --no-browser --port=8889
[I 14:52:43.384 NotebookApp] [nb_conda_kernels] enabled, 2 kernels found
[I 14:52:43.658 NotebookApp] [nb_conda] enabled
[I 14:52:43.660 NotebookApp] Serving notebooks from local directory: /home/bill
[I 14:52:43.661 NotebookApp] Jupyter Notebook 6.4.12 is running at:
[I 14:52:43.661 NotebookApp] http://localhost:8889/?token=6a196860b0b4efe5exxx0fee16d7d36e9ae4b781f620a5fc
[I 14:52:43.661 NotebookApp]  or http://127.0.0.1:8889/?token=6a196860b0b4efe5exxx0fee16d7d36e9ae4b781f620a5fc
[I 14:52:43.661 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
[C 14:52:43.663 NotebookApp] 
    
    To access the notebook, open this file in a browser:
        file:///home/bill/.local/share/jupyter/runtime/nbserver-53201-open.html
    Or copy and paste one of these URLs:
        http://localhost:8889/?token=6a196860b0b4efe5exxx0fee16d7d36e9ae4b781f620a5fc
     or http://127.0.0.1:8889/?token=6a196860b0b4efe5exxx0fee16d7d36e9ae4b781f620a5fc
```

And keep this terminal with `ssh` running open (when you hit ctrl+C, e.g. to get back the prompt, the Jupyter notebook is killed together with the `ssh` connection).  
Important: keep note of the "token" returned by the command: it is needed to login to the notebook.  
(in this case, it is `6a196860b0b4efe5exxx0fee16d7d36e9ae4b781f620a5fc`)

### Start the notebook independently of the `ssh` connection

If the terminal or the `ssh` connection is terminated or crashes or times out, the notebook server is also closed.  

#### Start the notebook in a VNC session

A solution could be to start the notebook server from a VNC session in a terminal (in which case, though, it will be closed if the VNC desktop is closed.  

#### Start the notebook with `nohup`

Another solution, to run it in the background, is to use the `nohup` command (see [here](https://gist.github.com/33eyes/e1da2d78979dc059433849c466ff5996)).  
Note that in this case there will be no output to the terminal to retrieve the token, so you need to look into `nohup` output file (usually `$HOME/nohup.out`), and to stop the notebook you need to find its PID (with `jupyter notebook list`) and use `jupyter notebook stop` or send the kill signal.  

**To start the notebook:**  
```
❯ nohup jupyter notebook &
[1] 55979
nohup: ignoring input and appending output to 'nohup.out'
```

**Then, to look at the command output you can use `tail`, for instance:**  
```
❯ tail -100 ~/nohup.out
[I 15:08:40.481 NotebookApp] [nb_conda_kernels] enabled, 2 kernels found
[I 15:08:40.618 NotebookApp] [nb_conda] enabled
[I 15:08:40.621 NotebookApp] Serving notebooks from local directory: /home/bill
[I 15:08:40.621 NotebookApp] Jupyter Notebook 6.4.12 is running at:
[I 15:08:40.621 NotebookApp] http://localhost:8888/?token=4b78e29e7ad2cxxx219344ba05af1a43c33c862f55db5113
[I 15:08:40.621 NotebookApp]  or http://127.0.0.1:8888/?token=4b78e29e7ad2cxxx219344ba05af1a43c33c862f55db5113
[I 15:08:40.621 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
[W 15:08:40.623 NotebookApp] No web browser found: could not locate runnable browser.
[C 15:08:40.623 NotebookApp] 
    
    To access the notebook, open this file in a browser:
        file:///home/bill/.local/share/jupyter/runtime/nbserver-55979-open.html
    Or copy and paste one of these URLs:
        http://localhost:8888/?token=4b78e29e7ad2cxxx219344ba05af1a43c33c862f55db5113
     or http://127.0.0.1:8888/?token=4b78e29e7ad2cxxx219344ba05af1a43c33c862f55db5113
```

**To look for running notebooks:**  
```
❯ jupyter notebook list
[NbserverListApp] WARNING | Config option `kernel_spec_manager_class` not recognized by `NbserverListApp`.
Currently running servers:
http://localhost:8888/?token=4b78e29e7ad2c23e219344ba05af1a43c33c862f55db5113 :: /home/bill
```

**To stop a running notebook:**  

```
jupyter notebook stop <port_number>
```

E.g:  
```
❯ jupyter notebook stop 8888
[NbserverStopApp] WARNING | Config option `kernel_spec_manager_class` not recognized by `NbserverStopApp`.
Shutting down server on 8888...
[1]  + 55979 done       nohup jupyter notebook
```

It is good practice to delete the nohup.out file regularly, since every time the output is appended.

## Setup a `ssh` tunnel

Needed to connect to the remote running notebook from the local browser.  

E.g: from local machine port 8889 to th Jupyter notebook on remote machine port 8888:  
```
❯ ssh -N -L localhost:8889:localhost:8888 bill@yoda
bill@yoda's password:
```

Because of the -N option, you won't get any output, except from errors.  
Keep also this terminal open, to keep the tunnel open.  

Note: in the redirection option, you can select any port on the local machine.  
`-L localhost:8883:localhost:8889` means that port `8883` on local machine is redirected to port `8889` on remote machine.  
The first port is the local machine, the second is the remote machine. The only binding one is the remote machine, which must correspond to the port where the notebook server is running.  

## Reach the notebook on a browser

In this example at the address `localhost:8889` (port defined in the first part of the `-L` option above).  
You'll be asked for the token to login.  

## Notes:

* Of course, edit machine address and user name as appropriate.  
* You can chose any port, provided it's not a privileged port (for which root is needed). If you don't specify it when the notebook is started, it should default to 8888 (it is anyhow shown in the terminal output), and that's the port you need to use in the `ssh` tunnel creation.  
