import sys


args = sys.argv
arg_cnt = len(sys.argv)

print(f'Type of argv: {type(args)}')
print(f'Length of argv: {arg_cnt}')
print(r'And they are:')
for itm in args:
    print(itm)
