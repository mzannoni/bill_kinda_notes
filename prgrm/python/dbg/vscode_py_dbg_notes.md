# Debugger in VSCode
## Useful to know
- use debug console and terminal side by side
- uncaught exceptions as break points
    - it breaks before exiting
    - look around at the status right before exiting
- debug with arguments
- env variables in config

### Main refs
- [Python debugging in VS Code](https://code.visualstudio.com/docs/python/debugging)

## Debug configurations examples

Python: current file with arguments, asked at debug start:  
```json
        {
            "name": "Python Debugger: Current File with arguments",
            "type": "debugpy",
            "request": "launch",
            "program": "${file}",
            "console": "integratedTerminal",
            "args": "${command:pickArgs}"
        },
```

Python: current file with arguments, input at debug start:  
```json
        {
            "name": "Python Debugger: Current File with fixed arguments",
            "type": "debugpy",
            "request": "launch",
            "program": "${file}",
            "console": "integratedTerminal",
            "args": "this_arg"
        },
```

Setting environment variable for debug:
```json
        {
            "name": "Python Debugger: Current File with env vars",
            "type": "debugpy",
            "request": "launch",
            "program": "${file}",
            "console": "integratedTerminal",
            "env":
            {
                "VAR_IN_ENV": "string_from_env"
            }
        },
```

To enable subprocess debugging set the option `subProcess` to `true` (default is `false`):  
```json
        {
            "name": "Python Debugger: Current File with arguments",
            "type": "debugpy",
            "request": "launch",
            "program": "${file}",
            "console": "integratedTerminal",
            "args": "${command:pickArgs}",
            "subProcess": true
        },
```
## Manual breakpoint in the code 

Call at any point in the code:   

```python 
debugpy.breakpoint() 
``` 

Ref: [Invoking a breakpoint in code](https://code.visualstudio.com/docs/python/debugging#_invoking-a-breakpoint-in-code)
