import os


env_l = os.environ

print(f'Do we have a 🏠: {'yes' if ('HOME' in env_l) else 'no'}.')
if 'HOME' in env_l:
    print(f'And that is {env_l['HOME']}.')


print(f'{'We\'re debugging!' if ('VAR_IN_ENV' in env_l) else 'We\'re just running.'}')
if 'VAR_IN_ENV' in env_l:
    print(f'We know because {env_l['VAR_IN_ENV']}')
