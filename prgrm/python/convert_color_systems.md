# Converting between color systems

There's a lib in Python that can be used to convert between color system (e.g. from HSL to RGB).  
It's called [`colorsys`](https://docs.python.org/3/library/colorsys.html#module-colorsys).  
