# Automatic method

Once the specific board library is installed, it is possible to run, with `sudo` the `post_install.sh` script to install the appropriate `udev` rules, so that it's possible ot upload a sketch as regular user.  

Example, after installing the lib for a UNO R4:  
```
sudo ~/.arduino15/packages/arduino/hardware/renesas_uno/1.3.2/post_install.sh
```

# Manually setup udev rules to allow the IDE to write to the device

As an alternative to the automatic setup script, the `udev` rules can be setup manually.  

Place these lines:  
```
# UDEV Rules for Arduino UNO R4 boards
#
# This will allow reflashing with DFU-util without using sudo
#
# This file must be placed in:
#
#       /etc/udev/rules.d
#
# After this file is installed, physically unplug and reconnect the device.
#
#       Arduino UNO R4
#       --------------
#
SUBSYSTEMS=="usb", ATTRS{idVendor}=="2341", ATTRS{idProduct}=="0069", GROUP="plugdev", MODE="0666"
SUBSYSTEMS=="usb", ATTRS{idVendor}=="2341", ATTRS{idProduct}=="0369", GROUP="plugdev", MODE="0666"
#
#       Arduino Uno R3 (CDC ACM)
#       --------------
SUBSYSTEMS=="usb", ATTRS{idVendor}=="2341", ATTRS{idProduct}=="0069", GROUP="plugdev", MODE="0666"
#
# If you share your linux system with other users, or just don't like the
# idea of write permission for everybody, you can replace MODE:="0666" with
# OWNER:="yourusername" to create the device owned by you, or with
# GROUP:="somegroupname" and mange access using standard unix groups.
#
```
in `/etc/udev/rules.d/99-arduino.rules`

Then:  
```
$ sudo udevadm control --reload
$ sudo udevadm trigger
```

For other boards, use the command `lsusb` once a board is connected to the USB to get the specific Product ID.  
