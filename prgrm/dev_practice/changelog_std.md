# Suggestion for standard for a CHANGELOG

Taken from [keepachangelog.com](keepachangelog.com).  

Every repo/project should keep a changelog.  
This is used to keep a curated files that stores changes to the repo, in chronological order.  
It is made for humans, not machines, so it doesn't make a lot of sense to make them automated.  

The main principle is to understand at each version what was changed, deprecated, added, etc.  

It should be organized in sections, ideally linkable, one for each version/release. Latest version on top.  

On the very top of the changelog there should be a section that collects all the changes since last version. In this way, keeping this section up to date each time a merge is done to the main branch, it is ready when a new version is released and there is lower chance to forget something.  

In each section, the changes should be divided into [tags](#types-of-changes-tags) that specify their type, for instance if they are additions, changes, deprecations, etc.  

## Example

A good format for a changelog is a markdown file.  
Following is an example of the content.

```
# Upcoming

## Added
- Documantation in Italian
- Function to replace blue with green

## Changed
- Reworked code of the video converting class


# v2.0.1

## Fixed
- Corrected conversion coefficient for thermal units in input reading function


# v2.0.0

## Added
- Test to verify thermal unit conversion function

## Removed
- Removed unused function for non-standard Imperial unit conversions

```

## Types of changes (tags)

* **`Added`**: new features and functionalities
* **`Changed`**: changes in existing functionalities and features
* **`Removed`**: to list removed features and functionalities
* **`Fixed`**: for bug fixes
* **`Security`**: fixed vulnerabilities
* **`Deprecated`**: to list features and functionalities that will be soon removed in upcoming releases
