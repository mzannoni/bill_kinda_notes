# Suggestion for commit message good standard

Taken from [Scipy development workflow](https://docs.scipy.org/doc/scipy/dev/contributor/development_workflow.html#writing-the-commit-message), with some personal adjustment, in particular to make all standard acronym of 3 characters.  

Example of commit message:
```
ENH: added function for W to mW conversion

Then some more details can be added, after one empty line.
This closes #1234
```

The first line starts with a standard 3 character acronym that specifies the type of commit. See [below](#standard-acronyms) for a list of valid acronyms.  
After this acronym some specific details should be added.  
Then, if necessary, additional text can be added, after one empty line.  

Each line in the commit message should be shorter than 99 characters.  
This can be set in Visual Studio Code commit message checking with the following two parameters in the user settings JSON:  
```
"git.inputValidationLength": 99,
"git.inputValidationSubjectLength": 99
```
The second controls the first line length, the first one controls every other line length.  

If the commit is related to a ticket/issue/etc. this can be explicitly stated with the `#` sign and the ID number of the ticket/issue/etc.  

## Standard Commit Tags

To be added at the beginning of the commit message.  
A tag could also be an emoji.  
| tag | emoji character | emoji short code | comment |
|-|:-:|:-|:-|
| API | 🔀 | :twisted_rightwards_arrows: | an (incompatible) API change |
| BCH | 📊 | :bar_chart:                 | change related to benchmarking |
| BLD | ⚙️ | :gear:                      | change related to building |
| BUG | 🐞 | :lady_beetle:               | bug fix |
| CFG | 🎛️ | :control_knobs:             | change in configuration or configuration files |
| DEP | 🕸️ | :spider_web:                | deprecate something, or remove a deprecated object |
| DEV | 💻 | :computer:                  | development tool or utility |
| DOC | 📖 | :book:                      | documentation (includes typo fix) |
| ENH | 💎 | :gem:                       | enhancement (new feature, functionality improvement, etc.) |
| MNT | 🛠️ | :hammer_and_wrench:         | maintenance commit (refactoring, folder structure, etc.) |
| RVT | ↩️ | :leftwards_arrow_with_hook: | revert an earlier commit |
| STY | 🎨 | :artist_palette:            | style fix (whitespace, PEP8) |
| TST | 🧪 | :test_tube:                 | addition or modification of tests |
| REL | 🚀 | :rocket:                    | related to preparing a release |
