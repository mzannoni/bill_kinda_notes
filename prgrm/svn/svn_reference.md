# svn useful commands

## get help
```
svn help
svn help <subcommand>
```

## resolve conflicts in current folder, forcing my current working copy to be the good version
```
svn resolve --accept mine-full .
```
- other options can be 
  - `mine-conflicts`
  - `theirs`
- with also the option `-R` it descends directories recursively

## have a look at the revision log
```
svn log
```

## copy part of the repository (actually, create branches)
```
svn copy ...
```

e.g. to create a branch:
```
svn copy http://svn.example.com/repos/calc/trunk http://svn.example.com/repos/calc/branches/my-calc-branch -m "Creating a private branch of /calc/trunk."
```

or in our case:
```
svn copy file:///opt3/PRJ file:///opt3/PRJ/V2.4.F -m "Creating a branch for version 2.4.F"
```

tag and branch are the same in svn:
```
svn copy file://opt3/PRJ/main file:///opt3/PRJ/tags/V2.4.F -m "Tagging version 2.4.F of the main project"
```

## check modifications with the default svn tool
```
svn diff
```

e.g. check modification from revision 100 to HEAD using an external tool (in this case 'meld'):
```
svn diff -r 100 --diff-cmd meld
```

## add a folder (or a file) to the ignore list
```
svn propset svn:ignore folder
svn propset svn:ignore file
```

## to get the status for a particular property on a file/folder use
```
svn propget
```

## to set the keywords Date and Revision (once these have been added inside the text file)
```
svn propset svn:keywords "Date Revision" file
```

## move files (either in the working copy or in the repo)
```
svn move SRC DST
```

when the case is `URL --> URL` this is equivalent to a complete server-side rename

## update working copy to a different URL
following command makes the current folder point to a new URL:
```
svn switch file:///opt3/PRJ/main .
```

## creating a dir in the repo:
```
svn mkdir -m "Making a new dir." http://svn.red-bean.com/repos/newdir
```
