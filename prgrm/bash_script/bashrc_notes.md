# add git branch status string to prompt

## use the function __git_ps1
- need to add a line to source it:  
`source /etc/bash_completion.d/git-prompt`  
  but note:  
  - when called with 0 or 1 argument it prints text to add to PS1 string  
    --> to be used like this in PS1 set  
  - when called with 2 or 3 arguments it sets PS1 variable: to be used with PROMPT_COMMAND  
    --> PS1 should not be one of the arguments! (or it would "grow" at every prompt, because it is repeatedly appended to itself)  

## PROMPT_COMMAND method:
- note: conda env seems to set PS1, so if the prompt is set with PROMPT_COMMAND it won't have conda strings  

Color: it seems that only in prompt command mode the git string is colored
But not using PS1 method would break conda functionality...  

## generic notes
- in the PS1 string, use '\W' instead of '\w' to show in the prompt only the current dir and not the entire path
