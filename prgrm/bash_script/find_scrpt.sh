#!/bin/bash
# folders=(
# vm
# backups
# data
# )
folders=(
bill
)
mkdir -p /tmp/find_home/
for folder in "${folders[@]}"; do
    echo "searching in $folder"
    find "/home/$folder" \( \
        -iname "*mont_blanc*" \
        -o -iregex ".*dataset_0[7-8].*" \
        -o -iregex ".*m[1-5]b.*" \
        -o -iname "*sees1*experiment*" \
        -o -iname "*europe*" \
    \) \
    > "/tmp/find_home/results_$folder_$( date +%Y%m%d_%H-%M-%S ).txt"
done
