## expansion

### This command:  
```
$ echo {one,two,three}
```
gives:  
`one two three`

### other examples:

```
$ echo word{this,that}otherword
```
`wordthisotherword wordthatotherword`

```
$ echo {a..c}{1..3}.file
```
`a1.file a2.file a3.file b1.file b2.file b3.file c1.file c2.file c3.file`

```
$ echo {2..10..2}
```
`2 4 6 8 10`

## example of listing (script content):
`*` is all the files in the current dir  
```
#!/bin/bash
for i in *
do
echo $i
done
```
### will give (depending on the current folder content)
```
Desktop
Documents
Downloads
Music
Pictures
Public
Templates
Videos
workspace
```

### useful links:
- http://www.thegeekstuff.com/2010/07/bash-string-manipulation/
- http://www.thegeekstuff.com/2010/06/bash-shell-brace-expansion/
- http://www.tldp.org/LDP/abs/html/string-manipulation.html
- https://www.shellscript.sh/


## Variables (preset):
- `$0`: basename of the program as it's called (to 'remove' the path: `basename $0`)
- `$#`: number of arguments (excluding the program name)
- `$1 ... $9`: first 9 arguments, to use more, use the shift command:  
    ```
    while [ "$#" -gt "0" ]
    do
      echo "\$1 is $1"
      shift
    done
    ```
- `$@`: all the arguments (preserves spacing with quoting)
- `$*`: all the arguments (but does not preserves spaces when quoting)
- `$?`: exit value of the last run command
- `$$`: PID if the currently running shell
- `$!`: PID of the last run background process
- `IFS`: internal field separator (default is SPACE TAB NEWLINE)
- `${var:-value}`: if var is not defined (or null), the expression evaluates to the specified default, in this case value
- `${var:=value}`: if var is not defined (or null), the variable var is set to the specified default, in this case value
