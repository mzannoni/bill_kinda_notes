# Easily plot a transfer function with octave

Contained in the package `control`  
```octave
>> pkg load control
```

Define a variable for a transfer function and a transfer function:  
```octave
>> s = tf('s')
>> G = (s + 1) / (s + 4)
```

## Exmaples

```octave
>> pkg load control
>> s = tf('s')

Transfer function 's' from input 'u1' to output ...

 y1:  s

Continuous-time model.
>> TF = (s*10 + 3) / (s^2 + 20*s + 5)

Transfer function 'TF' from input 'u1' to output ...

         10 s + 3
 y1:  --------------
      s^2 + 20 s + 5

Continuous-time model.
>> bode(TF)
```

```octave
>> pkg load control
>> s = tf('s')

Transfer function 's' from input 'u1' to output ...

 y1:  s

Continuous-time model.
>> R = 400e3
R = 400000
>> C = 20e-12
C = 2.0000e-11
>> G = (s*R*C) / (s^2*(R*C)^2 + 1)

Transfer function 'G' from input 'u1' to output ...

          8e-06 s
 y1:  ---------------
      6.4e-11 s^2 + 1

Continuous-time model.
>> bode(G)
```

# Similar approach using the `control` library in Python

Install the library (with `conda` or `pip`).  
```zsh
$ conda install control
```
or
```zsh
$ pip install control
```

Use the `TransferFunction` object to build the transfer function, and them use funcitons like `frequency_response()` or `step_response()` to generate vectors for plotting.  
Note that the plot needs to then be done manually, differently from `octave`.  

## example

```python
>>> import control
>>> import numpy as np
>>> import matplotlib.pyplot as plt
>>>
>>> num = [2, 3]
>>> den = [1, 4, 1]
>>> tf = control.TransferFunction(num, den)
>>> 
>>> mag, phase, omega = control.frequency_response(tf)
```
You can now use these vectors to sketch Bode plots with `matplotlib.pyplot`.  

Similarly, for the step response:  
```python
>>> t, y = control.step_response(tf)
```
And use these vectors to plot the step response.  
