## HARD RESET (network + microprocessor reset)

Although not listed in the Owner's manual, a "Hard reset" can be done using the below listed procedures which will not only reset the network card but will also do a microprocessor reset in the process and return all settings to their factory defaults.  

1. Turn off the power using ON/STANDBY.
2. Simultaneously press/hold the: S720W/S920W - "Tuner Preset Ch+" and "Tune -"; X1300/X2300/X3300W - "Zone 2 Source" and "Dimmer" ; X4300H/X6300H - "UP/DOWN" arrow buttons on the front panel and then press the ON/STANDBY button on the front panel.
3. Once the display starts flashing at intervals of about 1 second, release the two buttons (hold for at least 4-5 seconds).

** If in step 3 the display does not flash at intervals of about 1 second, start over from step 1.
