## HARD RESET
- Power off your device.
- Press the power key + volume down for a few seconds.
- Select English.
- Tap on Wipe data and cache.
- Tap on Wipe data and cache again.
- Tap on Yes to confirm data and cache wipe.
- Wait for the wipe process to complete.
- Reboot phone.
