# How to make a local WhatsApp backup

As of 2024 the WhatsApp backup via Google account won't be for free any more. It will count towards your maximum storage, and since it's likely at least a few GBytes, it will be a problem, for a free account.  

To make a local backup it should be sufficient to save the content of the folder `storage/emulated/0/Android/media/com.whatsapp`. Depending on the OS and mounting option, this folder could be (e.g. in Manjaro KDE): `<phone mount point>/Internal shared storage/Android/media/com.whatsapp/` (specific example: `mtp:/Fairphone 4 5G/Internal shared storage/Android/media/com.whatsapp/`).  
In there it should be stored the entire WhatsApp history and media.  

# Restore

According to some online guides, that I've not yet tested, it should be enough to copy back again this folder in a phone that has the same WhatsApp account and WhatsApp would recognize it and load everything.  

# Transfer to new phone

If the old phone is not lost, WhatsApp has now a function to transfer the entire history and media to a new phone, without having to use the Google account.  
Neither this feature has been tested, yet.  
