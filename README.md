# bill_kinda_notes

Collection of useful generic notes about many aspects of digital tools, and beyond.

## Content Index

### [backup](./bkp)
Backup tools and useful commands.

### [cad](./cad)
Useful notes and tips about CAD and EDA tools.

### [devices](./devices)
Tips, HOWTOs and tech tricks about miscellaneous HW devices (e.g. phones, AV stereo, etc.).

### [disk and filesystem](./disk_and_fs)
Notes about disk and partition management (mount, resize), storage devices and filesystem management and operations.  
There are also useful [notes about data rescue](./devices/rescue).

### [generic](./generic)
Unsorted but most useful tips and tricks.  
Sub-index of this section:  
* [AI](./generic/ai)
* [bill ASCII mark](./generic/bill_txt_mark)
* [compress and archives](./generic/compress_and_archives)
* [gedit](./generic/gedit)
* [image processing](./generic/image_edit)
* [kate](./generic/kate)
* [markdown](./generic/markdown)
* [miscellaneous](./generic/misc)
* [regex](./generic/regex)
* [various commands and tools for Linux](./generic/useful_commands_linux.md)
* [various commands and tools for Windows](./generic/useful_command_windows.md)
* [zsh](./generic/zsh): notes about zsh customization (not scripting)

### [network](./network)
Useful tools and commands for network test and management, including some tips for speed testing.

### [programming](./prgrm)
Notes about coding and software development.  
* [Arduino](.prgrm/arduino)
* [bash](./prgrm/bash_script)
* [development practices](./prgrm/dev_practice): includes suggestions for commit tags
* [git](./prgrm/git)
* [octave](./prgrm/octave)
* [python](./prgrm/python)
* [svn](./prgrm/svn)

### [system](./sys)
Notes and tricks about system and operative system management and maintenance.

### [virtual machine](./vm)
Notes on virtual machine management, like qemu or VirtualBox.
