# Enable markdownpreview in Kate

Two steps are needed (verified on Manjaro).  

1. Enable the 'Document Preview' plugin in `Settings --> Preview`
2. instal `markdownpart` package

Without this last installation the preview doesn't work.  
