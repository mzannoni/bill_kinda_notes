# tar

compress and extract with tar
`-z` if for gzip files

to compress:
```
$  tar -czf name_of_archive_file.tar.gz name_of_directory_to_tar
$  tar -czf name_of_archive_file.tar.gz name_of_file01_to_add name_of_file02_to_add file03 ...
```
actually - in front of czf is optional

to exclude something from the archive (e.g.):
```
$ tar -cvzf ../one_folder_above/name_of_archive_file.tar.gz --exclude="simulation" --exclude="calibre_*" --exclude="tpsco_*" --exclude="svdb" --exclude="*.gds" cds
```

to extract:
```
$  tar -xvf file.tar.gz
```
to extract in a different folder:
```
$  tar -xvf file.tar.gz  -C /path/to/directory
```

`-C` is the option to change folder

To add to the archive the files and folders specified in a text file:
```
$  tar -c -v -z -T list_of_files -f file.tar.gz
```
Note:
By default, each line read from the file list is first stripped off any leading and trailing whitespace. If the resulting string begins with `-` character, it is considered a tar option and is processed accordingly.  
For example, the common use of this feature is to change to another directory by specifying `-C` option:
```
$  cat list_of_files
-C/etc
passwd
hosts
-C/lib
libc.a
$  tar -c -f foo.tar --files-from list_of_files
```


to list the content of a tar file the option is `-t`
```
$  tar -tvf archive.tar
```

if it's a gzip, the option `-z` is also needed (or `-j` if it's a bzip2, `-J` if it's a xz and so on)
```
$  tar -ztvf archive.tar.gz
```

to limit depth of listing there's no specific option, but one can get a bit creative:
depth=1
```
$  tar --exclude="*/*" -tf archive.tar
```
depth=2
```
$  tar --exclude="*/*/*" -tf archive.tar
```


extract only a folder or file from an archive:
```
$  tar -xvf foo.tar home/foo/bar
```
where `home/foo/bar` is the path inside the tar  
note: no leading slash, paths are all relative in tar  
it is useful to list the content of the archive first (with `-t`) to know the path


compress and extract with 7z

adds all files from directory to archive.7z:
```
$  7z a -t7z archive.7z directory
```

same, splitting the archive into 2 GB volumes (create multi-volume archive):
```
$  7z a -t7z -v2g archive.7z directory
```

same, splitting into 6 GB volumes:
```
$  7z a -t7z -v6g /<abs_path_1>/archive.7z /<abs_path_2>/<folder>
```

--------

to use max compression with xz:
```
$  tar -I "xz -9" -cf archive.tar.xz files
```
you can also use this with 'pxz' and in combination with multi-thread options
```
$  tar -I "xz -9 -T4" -cf archive.tar.xz files
```

the option --totals can be used to show the total number of bytes written
```
$  tar -I "xz -9 -T4" -cf archive.tar.xz --totals files
```

--------
