create archive using multi-thread compression using tar and gz, bz2 or xz:

1) Pass explicit arguments to the tool called by tar:
```
$  tar -I "xz -T4" -cf archive.tar.xz files
```
to use 4 threads (from xz manual, "-T0" for max CPU count)

2) or install the parallel compression tools, which are:
* pigz
* pbzip2
* pxz

then instruct tar to use them in place of the standard ones, e.g.:
```
$  tar -I pxz -cf archive.tar.xz files
```
or to limit the number of threads:
```
$  tar -I "pxz -T4" -cf archive.tar.xz files
```

same applies for pigz and pbzip2, but specific options are different
