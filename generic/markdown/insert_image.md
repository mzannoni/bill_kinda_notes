# Markdown: insert images

## Basic

To add a basic local image use a `!` character followed by the alt text in `[]`and then the local address to the image in `()` (optionally, add a title in `""` inside the `()`):  
```
![My image](/local/path/to/img.png "optional title")
```

To add a link to an image, enclose the above in `[]` and add the link in `()`:  
```
[![My image](/local/path/to/img.png "optional title")](link_to_img)
```

However, this simple syntax doesn't allow many tweaks, e.g. the image size can't be change, no caption can be added, etc.

## With HTML

The `img` HTML tag can be used to add images (if the Markdown application supports HTML).  

For instance, to change size, the `width` and `height` attributes can be used:  
```
<img src="/path/to/img.png" width="200" height="100">
```

To add a caption, the `figure` and `figcaption` tags can be used:  

```
<figure>
    <img src="/path/to/img.png"
         alt="alt text">
    <figcaption>Caption to the figure.</figcaption>
</figure>
```
