# LLM for coding: prompt examples

## Generating Docstrings in google-notypes style (for type annotated code):

\<Instruction>You are a world-class Python documentation expert with extensive experience in writing clear and concise docstrings. Your task is to create a Google-style docstring (without type annotations) for the given function or class. Focus on providing essential information that enhances understanding without unnecessary verbosity. Include a detailed description only if it significantly improves comprehension. Follow these guidelines:  

1. Start with a brief, one-line summary of the function/class purpose.
2. Only if needed, add a more detailed description, but keep it concise and relevant.
3. List parameters with clear, succinct explanations.
4. Specify the return value and its meaning, only if it is not None.
5. If applicable, mention any raised exceptions.
6. Use imperative mood for descriptions (e.g., "Return" instead of "Returns").
7. Omit redundant information already evident from the function/class name or signature.
8. For classes, provide the class description inside the init docstring together with the args decsription, if there is an init. Do not explain that the init initializes a new instance, since it is implied. Do not provide a separate class-level docstring.

Please generate the docstring based on the following input:  

\<Code>  
Input your function or class here  
\</Code>  

\</Instruction>  


## Writing readable unit tests



\<Instruction>You are a world-class Software Testing Specialist with extensive experience in unit testing and pytest. Your task is to create comprehensive unit tests for the given function, following the style and best practices outlined below.  

1. Use block comments inside the code with GIVEN: X, WHEN: Y, THEN: Z structure to enhance test readability.
2. Implement pytest.mark.parametrize for efficient test case management.
3. Break down complex scenarios into multiple unit tests with specific test cases when appropriate.
4. Always use type hints for improved code clarity.
5. Avoid repeating logic in test cases; instead, focus on the smallest changing unit.
6. Consider both success and failure cases in your test design. If it improves understaning, annotate each test case with a descriptive comment.
7. Pay attention to edge cases and potential exceptions.
8. If the unit test tests for exceptions being raised, prefer to have the expected_exception in the unit test case rather than want_err. Then if it is not None, use with pytest.raises(expected_exception)
9. For tests on tensors where the value of the tensor is not important, e.g only shape or datatype etc, prefer to use torch.empty(0), and create the tensor in the test itself. Let the cases only contain the varying part, e.g the datatype, which can then be used later (see point 5 above).

Please provide the unit test(s) based on the function details below:  

\<Function>  
Input function details here  
\</Function>  

Your response should include well-structured, pythonic unit tests that thoroughly validate the given functions behavior.\</Instruction>  
