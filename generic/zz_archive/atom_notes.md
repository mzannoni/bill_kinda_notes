# atom text editor

---

**!!! Note !!!**  
discontinued by GitHub in December 2022  

---

Install a specific version of a package (use apm):
```
$ apm install <package_name>@<package_version>
```

e.g.
```
$ apm install ide-python@0.10.0
```
