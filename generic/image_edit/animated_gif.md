# w/ ImageMagick

ImageMagick can be used to easily create animated gif from the command line.  

This will create an animated gif with all the png files beginning with `proc_` in the current folder, with a delay of 1 second between layer.  
(The parameter for the delay is weird, it's ot tens of ms.)
```zsh
❯ magick -dispose previous -delay 100 -loop 0 proc_*.png proc_Si.gif
```

It is possible to have different delays per layer.  
The following gives a delay of 200 ms to all files from number `01` to number `11` and of 4 seconds to the last layer, image number 12.  
```zsh
❯ magick -dispose previous -delay 100 proc_{01..11}.png -delay 400 proc_12.png -loop 0 proc_Si.gif
```

To optimize the animation and make it a bit smoother:  
```zsh
❯ magick -dispose previous -delay 20 flip_{01..05}.png -delay 200 flip_06.png -loop 0 -interpolate Spline -layers OptimizePlus flip_stack.gif
```

This will introduce 10 layers between the given once and will sort of fade them in and out.  
```zsh
❯ magick convert -delay 20 -morph 10 flip_*.png flip_stack.gif
```

# w/ GIMP

With GIMP the layer can be simply used as frames of the animation.  
When exporting to gif it is possible to select to create an animation.  

To make different delay per layer with GIMP, add at the end of the layer name a string in brackets with the number of desired milliseconds and the string `ms`.  
A layer should have a name of the sort: `Layer1 (200 ms)`.
