# exiftool

add artist, creator, owner and copyright:
```
$ exiftool -Artist="Massimo Bill Zannoni" 34-enh.jpg
$ exiftool -OwnerName="Massimo Bill Zannoni" 34_enh.jpg
$ exiftool -Creator="Massimo Bill Zannoni" 34_enh.jpg
$ exiftool -Copyright="CC BY-SA 4.0" 34_enh.jpg
```

useful options:  
* `-ext jpg`: process files with jpg extension
* `-i directory`: ignore directory
* `-All=`: remove all tags (except unsafe tags): careful, some tags (e.g. color space specs) are used for proper rendering

full command:
```
$ exiftool -Artist="Massimo Bill Zannoni" -OwnerName="Massimo Bill Zannoni" -Creator="Massimo Bill Zannoni" -Copyright="CC BY-SA 4.0" -i done *.jpg
```

## Search based on file metadata

A few example to show how to search files, based on specific metadata.  

The basic command structure is something like:  
```
❯ exiftool -if '$<tag> <expression>' -r -filename <path>
```
* `<tag>` is the exif tag for which to look for a specific string ()for instance
* `<expression>` is  Pear-like expression applied to what's returned by the tag
* `<path>` specifies the folder
* `-r` searches recursively through subfolders
* `-filename` specifies to returns only the file name, otherwise the entire exif content for each of the matched files would be printed

### Examples
Searches for files that in the tag "Software" have the exact string "Snapseed 2.0".  
```
❯ exiftool -if '$Software eq "Snapseed 2.0"' -r -filename .
```

Searches for files for which the tag "Software" contains the string "Snapseed"  
```
❯ exiftool -if '$Software =~ "Snapseed"' -r -filename .
```
