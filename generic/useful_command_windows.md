### Calculate hash of a file

For instance in Powershell, `certutil.exe` can be used:  
SHA-1 hash:  
```
PS C:\Users\sidbi> certutil -hashfile <file> SHA1
```
SHA256:  
```
PS C:\Users\sidbi> certutil -hashfile <file> SHA256
```
