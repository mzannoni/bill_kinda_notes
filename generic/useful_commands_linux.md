# Linux useful commands

TODO: go through this file and see if some snippet can be moved to more appropriate notes

--------

Convert the man page for 'dd' into a pdf in the current folder named dd.pdf:
```
$  man -t dd | ps2pdf - dd.pdf
```
change 'dd' with any other desired manpage

--------

change quality to a pdf file:
```
$  gs -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dCompatibilityLevel=1.7 -dPDFSETTINGS=/screen -sOutputFile=output.pdf input.pdf
```

If the `-dPDFSETTINGS=/screen setting` is too low quality to suit your needs, replace it with -`dPDFSETTINGS=/ebook` for better quality, but slightly larger pdfs. Delete the setting altogether for the high quality default, which you can also explicitly call for with `-dPDFSETTINGS=/prepress`

alternative command (actually, it doesn't seem to work...):
```
$  convert -density 300x300 -quality 90 -compress jpeg input.pdf output.pdf
```

--------

encrypt a file with openssl:
```
$  openssl des3 -in filename -out whatever.des3
```

change des3 with whatever encryption algorithm you want  
for instance (used for OpenVPN certificates):
```
$  openssl rsa -in cert-orig.pem -out cert.pem -aes256
```

decrypt:
```
$  openssl des3 -d -in whatever.des3 -out whateveragain
```


or using gpg:

encrypt:
```
$  gpg --output whatever.data --symmetric --cipher-algo AES256 un_encrypted.data
```

decrypt:
```
$  gpg --output un_encrypted.data --decrypt whatever.data
```

--------

search inside folder 'cache' for files modified in the last 2 days and list them; then 'awk' implements a command:  
e.g.: add the 7th item of a line given by 'ls' previously to the variable 'sum' then print the value
```
$  find cache -mtime -2 -ls | awk '{sum += $7} END {print sum}'
```

--------

`useradd`: command to create a new user  
`usermod`: command to edit existing users

add a user to an existing group
```
#  usermod -a -G group-name usr-name
```
`-a` is important: it means 'append'; without this option, the user will be deleted from the groups not stated in the list

--------

convert an image to base64 format (e.g. to be embedded in html code):
```
$  cat image.png | base64
```

the output is the base64 conversion of the image

--------

Password protect a key (e.g. my_key.pem)
```
$  openssl rsa -in my_key.pem -out my_key_protected.pem -aes256
```

--------

Check CentOS release or version:

files:  
`/etc/centos-release`  
`/etc/os-release`

--------

convert a rpm into a deb to install on Ubuntu:

first, install alien

then:
```
$  sudo alien rpmpackage.rpm
$  sudo dpkg -i rpmpackage.deb
```

--------

Clear journal log (that takes space in /var/log/journal)
```
#  journalctl --vacuum-time=1w
```
keeps only the last 1 week of journal log; you can specify time with s, m, h, month, d, etc

```
#  journalctl --vacuum-size=1G
```
keeps only the last 1G worth of journal log; you can use K, M, etc to specify size to keep

--------

`head` and `tail` commands in bash (to read a file from the beginning or the end)

get the last 10000 lines of a file:
```
$  tail -n 10000 file
```

follow a file while it's being written (e.g. a log file):
```
$  tail -f file
```

--------

cleanup unused flatpak runtime envs
```
$  flatpak uninstall --unused
```

better to run it NOT as root

if not all is cleaned up:

```
$  flatpak repair
```

flatpak still keeps old environments, by design; you must uninstall them manually  
look at them with

```
$  flatpak list
```

then, to remove more than one at a time, wildcard characters don't work in the syntax, but if you just partially specify the name of the package to uninstall, flatpak will ask what to do, with one option to uninstall all those that have the same name beginning in common

```
$  flatpak uninstal org.freedesktop.
```


to update the flatpak apps and env:
```
$  flatpak update
```

--------

to change logrotate behavior (esp. to enable compression of messages in /var/log/) edit:
```
  /etc/logrotate.conf
```

--------

restart Gnome Shell (e.g. if some extension crashes)

    <Alt+F2>
that opens the command prompt; enter the command

    r
and `<Enter>`

It should restart the Gnome Shell without loosing any work (all programs reopens at their status)

--------

To check bitness of an executable, the command 'file' can be useful.  
This command actually proveides info about the type of file, for an executable it tells many useful info  
e.g.
```
  bill@yoda:bin$ file ShadowOfTheTombRaider
  ShadowOfTheTombRaider: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=5559bd11cfb1cae534d961a060a60440fbe03328, for GNU/Linux 2.6.24, stripped
  bill@yoda:bin$
```

--------

clamAV

update virus database:
```
#  freschclam
```
scan <file/dir> (following directory tree), print only infected, move infected to DIR, detect possibly unwanted application, scan into archives:
```
$  clamscan -r -i --move=DIR --detect-pua=yes --scan-archive=yes <file/dir>
```
scan the whole system, printing only infected:
```
#  clamscan -r -i /
```

--------

Open Brave with a specific profile (useful, for instance, to set in IDE, like PyCharm, to open doc links in the coder profile)
```
$  brave-browser --profile-directory="Profile 3"
```

To open a URL in a new tab:
```
$  brave-browser --profile-directory="Profile 3" <url>
```

Unfortunately, it's not trivial to figure out which profile number correspond to which profile name (trial and error)

--------

Install fonts in addition to double click on the file in the file explorer, e.g. when you want to install many at a time:  
place fonts in `~/.local/share/fonts` (where they go when installed) or also in `~/.fonts`.  
There really seems to be not much more than this... once they're there, they're available.  
(The above has been verified only in Ubuntu.)

--------

Find out what is the current shell.  
To find out what shell is running, one way it to use `ps` with the option `-p` on the PID of the currently running process, which is stored in the special variable `$`.  
```
  $ ps -p $$
```

For instance:  
```
❯ ps -p $$
    PID TTY          TIME CMD
  59265 pts/0    00:00:02 zsh
❯ bash
bill@yoda:bill_kinda_notes (mz/on_yoda)$ ps -p $$
    PID TTY          TIME CMD
  65248 pts/0    00:00:00 bash
```

--------

Enable or disable screen-lock (in Gnome)

At times it happens that the screen lock feature doesn't work, and this may be because it doesn't work.  

To first check if it works:  
```
❯ xdg-screensaver lock
```

If the screen doesn't lock, check the GSettings relative setting:  
```
❯ gsettings get org.gnome.desktop.lockdown disable-lock-screen
true
```

If this returns `true` as above, it means that the screen lock is disabled (it could be because of some application that did it, even if temporary).  
To enable the lock again just change this setting:  
```
gsettings set org.gnome.desktop.lockdown disable-lock-screen false
```

All these settings can also be viewed and edited graphically with `dconf Editor`.  

Open it and navigate to `org/gnome/desktop/lockdown`, in this specific case.  

--------

Change permissions to all files in the current directory and all subdirectory, withouot changing folder:
(in this specific case, execution permission is removed for all)
```
❯ find . -type f -exec chmod a-x {} \;
```
--------

find empty folders:
  $ find . -type -empty
 
to also delete:
  $ find . -type -empty -exec rmdir {} \;

--------
