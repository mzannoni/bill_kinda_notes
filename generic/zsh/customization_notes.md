Install [oh-my-zsh](https://github.com/ohmyzsh/ohmyzsh/) following the instructions there on the website.

Append at the end of the `.zshrc` file created by oh-my-zsh the customization created by zsh installation (e.g. [`zshrc_basic`](./zshrc_basic), the first one I've created upon installation of zsh).
```
$ cat ~/.zshrc.pre-oh-my-zsh >> .zshrc
```
so, in my specific case:
```
$ cat ~/.zshrc_basic >> .zshrc
```

Make sure the `PATH` is the same as in bash (check what is not loaded in `.profile` and only in `.bashrc`).

Conda folder will not be in the path. To init conda, launch the `conda activate` command in the conda folder, and it will be prompted to do so.
```
$ /home/bill/anaconda3/bin/conda activate
```

If that doesn't work, you can explicitly launch `conda init`:
```
$ /home/bill/anaconda3/bin/conda init
```

and if that somehow initialize another shell (it happened...) you can explicitly specify what shell to init:
```
$ /home/bill/anaconda3/bin/conda init zsh
```

To have autocompletion in conda, the `conda-zsh-completion` plugin (for instance) is needed:  
https://github.com/esc/conda-zsh-completion

It is not yet part of oh-my-zsh default plugin set, but it can be added to the custom plugin folder (defined by `$ZSH_CUSTOM/plugins`, by default `~/.oh-my-zsh/custom/plugins`)

So you can simply clone the repo of the plugin in there, and then add it to the `plugin(...)` section in the `.zshrc`.
```
$ git clone https://github.com/esc/conda-zsh-completion ~/.oh-my-zsh/custom/plugins/conda-zsh-completion
```
or if you're already in zsh:
```
$ git clone https://github.com/esc/conda-zsh-completion $ZSH_CUSTOM/plugins/conda-zsh-completion
```

and then in the `.zshrc`:
```
plugins=(… conda-zsh-completion)
```

Another interesting tool to check out, to better manage external plugins is `antigen`: https://github.com/zsh-users/antigen
This is somehow actually something even more than oh-my-zsh, a full manager for zsh customization. I haven't tried it yet.


A very interesting **theme** to checkout: https://github.com/romkatv/powerlevel10k  
Plugin for **auto suggestions** as you type (from history, etc.): https://github.com/zsh-users/zsh-autosuggestions  


**Rust**  
To add Rust completion features:  
- If oh-my-zsh is installed, the easiest is just to enable the `rust` plugin.  
- Otherwise:  
create the directory `~/.zfunc`
Then run:
```
$ rustup completions zsh > ~/.zfunc/_rustup
```
and then add the line
```
fpath+=~/.zfunc
```
to the `.zshrc` file, before the `compinit` (i.e. before sourcing the oh-my-zsh script).

**useful aliases**

Aliases can be easily set with `oh-my-zsh`.  
These lines are in a file in the `$ZSH_CUSTOM` folder:  
```zsh
alias lll="ls -alh"
alias ..="cd .."
alias now="date +%T"
```
