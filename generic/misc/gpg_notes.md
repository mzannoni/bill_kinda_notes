# gpg command

export public key:
```
$ gpg2 --armor --export _keyID_ > mykey.asc
$ gpg2 --output mykey.gpg --export _keyID_
```

export private key: (careful) [e.g. specify an output file in an ecrypted partition]
```
$ gpg2 --armor --export-secret-keys _keyID_ > myseckey.asc
$ gpg2 --export-secret-keys _keyID_ > myseckey.gpg
```

export and directly encrypt:
```
$ gpg2 --export-secret-keys _keyID_ | gpg -c > encrypted
```

create revoke certificate:
```
$ gpg2 --output revoke.asc --gen-revoke _keyID_
```

import:
```
$ gpg2 --import mykeyfile
```

after importing it, you need to add the trust level:
```
$ gpg2 --edit-key (keyID)
gpg> trust
```

and give a 5, if you trust yourself...

encrypt a file:
```
$ gpg --symmetric file
```
outputs file.gpg

to decrypt:
```
$ gpg --decrypt file.gpg > file
```

Encrypt (for a certain recipient for whom I have the pub key):
```
$ gpg --output doc.gpg --encrypt --recipient blake@cyb.org doc
```
or
```
$ gpg --encrypt --sign --armor -r mary-geek@protonmail.com doc.txt
```
with this last one, the file will have the same name as the input, but with .asc extension appended

Decrypt:
```
$ gpg --output doc --decrypt doc.gpg
```
or
```
$ gpg --decrypt coded.asc > plain.txt
```

Verify signature
```
$ gpg --verify <filename>.sig <filename>
```
for this to be working, make sure you have already imported the public key used to sign.
