# If you forget the pwd of a pdf

There are 2 command line options:  

## `pdfcrack`

Available in the software repos in Manjaro, for instance.  

### examples

```zsh
❯ pdfcrack -f filename.pdf
```
Brute-force, starts from words of 4 characters.  

It is of course possible to offer a file with list of strings to try.  

## `pdfrip`

This one is interesting because multi-thread.  
It's written in Rust, provided through `cargo`.  

https://github.com/mufeedvh/pdfrip

### examples

```zsh
❯ pdfrip -f filename.pdf default-query --max-length 16
```
This brute-force the file by trying string between 4 and 16 characters.  

It is also possible to limit the search by defining patterns etc.  
