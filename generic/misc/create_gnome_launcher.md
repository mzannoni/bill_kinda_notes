# Create Gnome launcher

(e.g. for Eclipse 4.7)

```
# vim /usr/share/applications/eclipse-4.7.desktop
```

And write inside:
```
[Desktop Entry]
Encoding=UTF-8
Name=Eclipse 4.7
Comment=Eclipse Luna
Exec=/usr/bin/eclipse
Icon=/usr/eclipse/icon.xpm
Categories=Application;Development;Java;IDE
Version=1.0
Type=Application
Terminal=0
```
