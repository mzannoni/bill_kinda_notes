# Speech-dispatcher

It's a tool that allows text to speak.

## Command line

Use `spd-say "hello"` for instance to have the computer pronounce the string "hello".

## Manjaro

These were the packages that needed to be installed:
```
speech-dispatcher
espeak-ng
```

In particular, the tool was not working, just reproducing an error message all the time, until `espeak-ng` was installed. It is actually among the recommended dependencies of `speech-dispatcher`.

The reason was that `speech-dispatcher` needs an output module to be installed in order to synthesize speech. `espeak-ng` is one of these modules. To list available output modules: `spd-say -O`.

## Basic examples

To simply reproduce something:
```
spd-say "the sentence you like"
```

Long messages tend not to be completely pronounced but rather cut at a certain point. The option `-w` makes sure that the prompt is not returned until the message is finished.
```
spd-say -w "the sentence you like"
```

There are also many other option, e.g. to change language or voice for the synthesis: `spd-say --help`.
