# grep command

To show lines before and after the line with matching pattern use options -B <n> and -A <n> respectively:

```
$ ls -alh | grep -B 2 -A 2 bash
```

Show the output of ls limited to lines matching the pattern ('bash' in this case), plus 2 preceding and 2 following lines, for each of the matchin lines to be shown.


By default, grep considers pattern as basic regex (default option -G).  
With -F the pattern is intepreted as fixed string.

The option -o only prints the matched pattern, one per line.  
The following prints the patterns (not the entire line containing it) in FileName lines that end in 'ptrn':
```
$ grep -o ".*ptrn" FileName
```
With $ in the pattern def, only matches such patterns that are at the end of a line:
```
$ grep -o ".*ptrn$" FileName
```

With sort, the output is then sorted, with the option -u (stands for "unique"), matching patterns are shown only once:
```
$ grep -o ".*tsmcN22" FileName | sort -u
```
