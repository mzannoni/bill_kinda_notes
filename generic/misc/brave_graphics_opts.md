# Check GPU graphics in Brave

Go to the page `brave://gpu`.

# Settings experimental features

In Brave Browser experimental features can be set at the address `brave://flags`, where many options are exposed and they allow to test experimental features.

# Enable Vulkan as the graphics backend

Set the flag `#enable-vulkan` to "Enabled".

# Enable WebGPU: Unsafe

Set the flag `#enable-unsafe_webgpu` to "Enabled".

This works if also the backend is set to `Vulkan`.
