# Convert Kindle files to PDF

## Download the books from the Kindle PC app

Important: newer versions of Kindle app use a format (KFX) that can't be converted.  
One solution (the only one?) is to use an old version of Kindle. It is important to prevent it from auto-update.  
To find a rpevious version (best is v1.17) you can look online for this specific file name and relative SHA256:  
KindleForPC-installer-1.17.44170.exe 14e0f0053f1276c0c7c446892dc170344f707fbfe99b695176 2c120144163200  
Extensive reference: https://www.mobileread.com/forums/showthread.php?t=283371  

Possible issues:  
- you can't sign in (especially if you installed a previous version over an newer one):  
	-- try to sign out the PC device on the Kindle online account settings and then sign in again  
- it's hard to prevent auto-update:  
	-- replace the `update` folder within you Kindle for PC installation with a file with the same name: Kindle should now be unable to download the upadted  

Then, open Kindle and download the books form your library.  
They are (by default) saved to `C:\Users\<username>\Documents\My Kindle Content`.  


## Convert to PDF

Use [Calibre](https://calibre-ebook.com/) with the [DeDRM plugin from apprenticeharper](https://github.com/apprenticeharper/DeDRM_tools).  
Once the DeDRM plugin is installed, the books are decrypted when imported into Calibre. You may need to remove them and import them again.  

You can then use Calibre to convert to PDF.

### References

* https://www.techradar.com/how-to/how-to-convert-a-kindle-book-to-pdf
