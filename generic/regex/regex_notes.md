# RegEx notes

useful ref:  
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions/Cheatsheet

online tool to test expressions:  
https://regexr.com/


## Using regex to find and replace in Atom or in Visual Studio Code

To match a line starting with `'  $'` and replacing it with the same line, except the `'  $'` string, preceded and followed by specific strings:  

in 'Find' filed set:  
`^\s\s[$](.*)`  
in 'Replace':  
` ```\n$ $1\n``` `

This would replace this line:
```
  $ clamscan -r -i --move=DIR --detect-pua=yes --scan-archive=yes <file/dir>
```
with these 3 lines:
````
```
$ clamscan -r -i --move=DIR --detect-pua=yes --scan-archive=yes <file/dir>
```
````
(useful to convert some text to Markdown)  

**Explanation:**  
Find string:  
| | |  
|--:|:--|  
| `^` | is to match the beginning of the line |  
| `\s` | matches one white space |  
| `()` | create a numbered group that can then be addressed later (with $1 for the first matched group and so on) |  
| `.` | matches any character, except `\n` |  
| `*` | used to match the preceding element any number of times |  

Replace string:  
3 times the character `` ` ``, then a new line, than the 1st group matched in the "Find" string, then again a new line and 3 times `` ` ``  

Useful reference: https://docs.microsoft.com/en-us/dotnet/standard/base-types/regular-expression-language-quick-reference

## in Python

**Example**  
Try this:  
```
import re  

pattern = re.compile(r'(?=.*apple)(?=.*orange)', re.IGNORECASE)
result = pattern.match(your_string)
```
