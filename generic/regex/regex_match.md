# Regular Expression matching notes

to match a string of 4 numeric character, all between 0 and 9:  
```
[0-9][0-9][0-9][0-9]
```

this matches strings like `"0098"`, `"3453"` and so... but not strings of less characters like `"098"`

`.` (period)  
normally (when not escaped or used in character class) matches any single character except new line  

`*` (asterisk)  
matches preceding character 0 or more times  

`n[5-9]c`  
would match any string starting with `n`, having one single digit comprised between 5 and 9 included, ending with `c`
