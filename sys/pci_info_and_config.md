# PCI hardware notes

show PCI devices as tree:
```
$ sudo lspci -t
```

show PCI slots:
```
$ sudo dmidecode -t slot
```

---
