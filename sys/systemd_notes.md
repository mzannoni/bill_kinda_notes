# `systemd` notes

## Basics

Unit: basic object to be managed  
Base command: `systemctl`  

`systemctl list-units`: (default of systemctl)  
`systemctl list-unit-files`  


`journalctl`: see the log entries  
- `-b`: from current boot
- `-k`: kernel messages (like dmesg)
- `-u <unit>`

Target: similar to `runlevel`, but more then one can be active at a time  
(e.g. shutdown or reboot are targets; the command reboot is an alias for calling the target reboot)  

the default target is made through a sym link to `/etc/systemd/system/default.target`  


`systemd-analyze blame`: info on units' boot time

## Control systemd on a remote machine (uses ssh)
```
systectl --host user@host command_for_systemd
```

## Unit files
- Unit files distributed with software packages:  
  `/usr/lib/systemd/system/`  
- Unit files created or customized by the administrator user:  
  `/etc/systemd/system/`  


### Creating new unit:

- prepare the executable
- create the the unit file (`something.service`) in `/etc/systemd/system`
- open `something.service` and edit it
- run: '`systemctl daemon-reload`' and optionally '`systemctl start name.service`'

## Timers

Timers: units for scheduling jobs  
for each `.timer` file a matching `.service` file exists (it is possible to control units with different name using the Unit= option)  
the `.timer` file activates and controls the `.service` file  

`OnCalendar=` syntax examples:
```
# DayOfWeek Year-Month-Day Hour:Minute:Second
# '*' means any, ',' used to list values, '..' to define continous ranges

# the follow make the service run the first 4 days of each month at 12pm, but only if the day is also on a Monday or a Tuesday
OnCalendar=Mon,Tue *-*-01..04 12:00:00

# runs every week (precisely Monday at 12am); option Persistent=true makes it run once immediately, if it missed the last run
OnCalendar=weekly
Persistent=true
```

`RandomizedDelaySec` option: randomize the time skew of precise time events like weekly or daily (avoid services to run at the same time, if they are set to run daily, weekly, monthly, etc)

---
ref:  
- https://wiki.archlinux.org/index.php/systemd
