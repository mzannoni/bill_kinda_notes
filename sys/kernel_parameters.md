# See a list of current paramters
To check the parameters the system was booted with:  
```
cat /proc/cdmline
```
# GRUB
## Temporary add paramters at boot
Press `e` when the menu shows and add them to the `linux` line, e.g.:  
```
linux /boot/vmlinuz-linux root=UUID=0a3407de-014b-458b-b5c1-848e92a327a3 rw btusb.enable_autosuspend=0
```
to add a parameter to disable Bluetooth autosuspend.

## Permanently add paramters to boot loader entries
Edit the file `/etc/default/grub` and append the parameters between quotes (`"`) in the `GRUB_CMDLINE_LINUX_DEFAULT` line, e.g.:  
```
GRUB_CMDLINE_LINUX_DEFAULT="btusb.enable_autosuspend=0"
```
and then automatically rebuild the `grub.cfg` with the command:  
```
sudo grub-mkconfig -o /boot/grub/grub.cfg
```

## references
https://wiki.archlinux.org/title/kernel_parameters
