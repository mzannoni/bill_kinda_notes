# `apt` notes

Use `apt` to list current and available versions of a package:  
```
$ sudo apt policy <package>
```

A similar effect can be optained with  
```
$ sudo apt-cache policy <package>
```
or  
```
$ sudo apt-cache madison <package>
```
The last one gives a somewhat less detailed output.  

To install a specific version of a package (e.g. to downgrade to a previous version):  
```
$ sudo apt install <package>=<version>
```
or  
```
$ sudo apt-get install package=version -V
```
the `-V` param helps to get some more details about the transaction.  

Note: it may not be always possible to cleanly downgrade to a previous version, e.g. if some dependencies can't be met. In that case, the output of the commands helps to understand which other packages have to also be downgraded.

---
