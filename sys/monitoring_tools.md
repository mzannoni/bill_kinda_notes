# Sensors

Install `lm_sensors` (in Manjaro) or `lm-sensors` in Ubuntu and then run:  
```
❯ sudo sensors-detect
```
And follow the instructions.

After that, available sensors data can be accessed with:  
```
❯ sensors
```

A useful graphical tool, also base on `lm_sensors` is `psensor`.

# GPU tools

## `GPU Viewer`

Provided as `flatpak` in Manjaro, seems to be a very useful tool.  

## Ricks-Lab GPU utils

For non-Ubuntu distros it's available through PyPI:  

```
❯ pip3 install rickslab-gpu-utils
```

Then to use it, useful instructions [here](https://github.com/Ricks-Lab/gpu-utils/blob/master/docs/USER_GUIDE.md#getting-started).  

## AMD GPU tools

Command line tool, similar to `top`: `radeontop`.  

~~Grahpical tool: `radeon-profile`.~~ This seems to be an old project, the GitHub project is not updated since a few years and doesn't work with newest GPUs.  



