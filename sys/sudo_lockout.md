# sudo password doesn't work

It may happen that an account is temporarily locked out (of `sudo` permissions) due to failed attempts.  
The command `faillock` can be used, as superuser, to check and reset this.  

Check `faillock` status:  
Switch to superuser with `su`, then  
```
# faillock --user <yourusername>
```

To reset, if necessary:  
Switch to superuser with `su`, then  
```
# faillock --user <yourusername> reset
```
