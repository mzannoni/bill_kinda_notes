# `yum` notes

List available version of a package:
```
# yum list <package_name> --showduplicates
```

Install a specific version of a package:
```
# sudo yum install <package_name>-<version_info>
```

Force yum to downgrade a package to a specific version:
```
# sudo yum downgrade <package_name>-<version_info>
```

---

Exclude packages from update:
```
# yum update --exclude=systemd* --exclude=firefox*
```
it will exclude all packages starting with systemd or firefox from the updates. An –exclude= option is required for every pattern to match.

To make this permanent, add a line at the end of /etc/yum.conf:
```
exclude=systemd* libgudev1* tigervnc*
```

---

List packages installed from a specific repo:
```
# yumdb search from_repo reponame
```

---
