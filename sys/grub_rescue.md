# Set of tricks used to rescue `grub`

## Manjaro

The last time it happened, was because I added a `/boot` partition and somewhat, even if I did update grub with the new partition mounted, it didn't work out.  
I found the solution [here](https://wiki.manjaro.org/index.php?title=GRUB/Restore_the_GRUB_Bootloader).  

Specifically, the steps that worked (the automatic script didn't work, apparently it looked for partitions and OSs only in regular disks, not in the nvme ones):

* Start with a Manjaro live USB
* Connect to the Internet
* use `gparted` to get the names of the 3 partitions of interest (the names here are just an example)
    * /dev/part1: /boot/efi
    * /dev/part2: /boot
    * /dev/part3: /
* mount the 3 partitions in the correct sequence
    * root first, in my case with a btrfs FS
        ```# mount -o subvol=@ /dev/part3 /mnt```
    * boot second
        ```# mount /dev/part2 /mnt/boot```
    * efi partition last
        ```# mount /dev/part1 /mnt/boot/efi```
* create `chroot` env and reinstall
    ```
    # manjaro-chroot /mnt /bin/bash
    # pacman -Syu grub
    # grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=manjaro --recheck
    # grub-mkconfig -o /boot/grub/grub.cfg
    ```
    Don't worry if the step with `pacman` gives some errors, but it's important it manages to install and up-to-date `grub`

At this point, I could power down, remove the USB stick and power up again and it worked.  
The was no Windows option in the boot menu, so in Manjaro I mounted the Windows partition and the run `update-grub` again.  
