# Useful commands for hardware

to find info about the hardware (read the DMI table, also called SMBIOS)  
as su:  
```
# dmidecode
```
man gives useful info

e.g. with the option `--type` only some specific info is shown

to show info about power supply:
```
# dmidecode --type 39
```
all types are described in the man page

---

check status of an SSD disk  

command: `smartctl` (need to install `smartmontools`)  

get info:
```
$ sudo smartctl -i /dev/sdX
```

get results from tests:
```
$ sudo smartctl -a /dev/sdX
```

to run the short test and get the results:
```
$ sudo smartctl -t short -a /dev/sdX
```
and the long one:
```
$ sudo smartctl -t long -a /dev/sdX
```

the entry "`Percentage Used`" should give an idea how much lifetime has passed (100% equals SSD at end of life)  

references:  
- https://askubuntu.com/questions/325283/how-do-i-check-the-health-of-a-ssd (but not the first answer)
- https://linuxhandbook.com/check-ssd-health/

---
