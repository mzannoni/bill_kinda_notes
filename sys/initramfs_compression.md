Especially in case the boot partition turns out to be too small, before resizing, it's possible to try and improve compression of the initramfs files.

Edit the file `/etc/initramfs-tools/initramfs.conf` and change the line `COMPRESS=gzip` to:
```
COMPRESS=xz
```

Then to rebuild the images:
```
$ sudo update-initramfs -u -k all
```
