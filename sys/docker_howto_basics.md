## Tutorial:
https://docs.docker.com/get-started/

## install
https://docs.docker.com/engine/installation/linux/centos/#install-using-the-repository

## start docker
```
# systemctl start docker
```

## enable docker service (start at machine start-up)


```
# systemctl enable docker
```

## list images on your system
```
# docker images
```

## show containers on the system
```
# docker ps -a
```

## stop a container
```
# docker stop container_name_or_id
```

## stop all containers
```
# docker stop $(docker ps -a -q)
```

## remove a container
```
# docker rm container_name_or_id
```

## remove all containers
```
# docker rm $(docker ps -a -q)
```

## copy a file from a container to the host
```
# docker cp <containerId>:/file/path/within/container /host/path/target
```

## list the docker networks
```
# docker network ls
```

## inspect a network
```
# docker network inspect bridge
```

## disconnect a container from a network
```
# docker network disconnect network_name container_name
```

## create a new network
```
# docker network create -d bridge new_bridge_network
```
'`-d bridge`' is optional, because bridge is the default  
when you start a container, pass the '`--net=new_bridge_network`' flag to connect the container to the net  

## connect a container to an existing network
```
# docker network connect new_bridge_network container_name
```

## adding a data volume
use the '`-v`' flag with the '`docker create`' or '`docker run`' commands  
```
-v /volume_name
```
creates a new volume inside a container at '/volume_name'  

## mounting a host directory as a data volume
the flag '`-v`' can be used to mount a directory from the host to the container  
```
-v /directory_on_host:/volume_on_container
```
mount the `/directory_on_host` from the host to the `/volume_on_container` directory in the container

---

## useful links
- about '`docker run`' command:  
  https://docs.docker.com/engine/reference/run/
- about 'docker start' command  
  https://docs.docker.com/engine/reference/commandline/start/
- container for mediawiki  
  - https://hub.docker.com/r/kristophjunge/mediawiki/
  - https://store.docker.com/community/images/synctree/mediawiki
