# How to edit display settings in the login screen

The settings for the display have to be added to gdm3 home folder.  

One simple way is to copy the settings of a user session to the gdm3 home folder.  

After settings up as preferred in `Settings > Displays`, copy the settings to gdm home:
```
$ sudo cp ~/.config/monitors.xml ~gdm/.config
```

Also, make sure that the following line in `/etc/gdm3/custom.conf`
```
WaylandEnable=false
```
is uncommented.  

The same principle applies in case you want a configuration for the login screen, different from the regular user session (e.g. to set mirroring by default in case you often change monitors setup).  
Create the configuration you want with the GUI settings, then save it. Copy the generated `monitors.xml` to gdm home, then restore the previous configuration for yor session (e.g. you can make a copy of `monitors.xml` before setting the configuration for the login screen).  

## Additional notes about gdm3:  

`~gdm` should actually point to `/var/lib/gdm3`.  

It is actually a known bug in gdm:  
https://gitlab.gnome.org/GNOME/gdm/-/issues/372
