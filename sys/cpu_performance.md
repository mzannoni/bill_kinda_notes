A CPU governor can be used to set performance.

in Ubuntu, install `cpufrequtils`

```
$ sudo apt-get install cpufrequtils
```

and set the params:
  sudo nano /etc/default/cpufrequtils
and add the line
```
GOVERNOR="performance"
```

then restart the service:
```
$ sudo systemctl restart cpufrequtils
```

to check:
```
$ cpufreq-info
```

there are also other commands available like `cpufreq-set` or `cpufreq-selector`.

Another useful GUI tool, possibly in combination, is `cpupower-gui` (for Gnome).
