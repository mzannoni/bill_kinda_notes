# `rpm` notes

List all packages (only package names):
```
$ rpm -qa
```

List all packages (more details):
```
$ dnf list installed
```

Install from a file list (e.g. list.txt):
```
# dnf install $(cat list.txt)
```
