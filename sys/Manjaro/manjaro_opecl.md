# OpenCL in Manjaro with AMD GPU

# AMD provided

## `rocm-opencl-runtime`

~~These don't seem to work perfectly. For instance, `darktable` doesn't run if this package is installed.  ~~
After version 5.7 this also seems to work fine.  

# Community provided

## `opencl-amd`

OpenCL can also be provided by this package and it seems to work equally well.  

# Comparison

Performed with a `phoronix-test-suite` test suite:  
https://openbenchmarking.org/result/2311085-BILL-231107406&hgv=20231107_postswitch_mjrperf_opencl_cmnt%2C20231107_postswitch_mjrperf_opencl_rocm&ppt=D&rmm=20230927-trial%2C20230928_preswitch%2C20231020_postswitchperf&hgv=20231107_postswitch_mjrperf_opencl_cmnt%2C20231107_postswitch_mjrperf_opencl_rocm&ppt=D
