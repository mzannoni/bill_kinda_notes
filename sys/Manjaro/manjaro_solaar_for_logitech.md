# Enable module for Solaar

Solaar is a software that can be used in Linux to manage Logitech receivers and connected devices.  

In Manjaro it doesn't seem to work out of the box. A module needs to be added to `/etc/mkinitcpio.conf`.  

Add this line to that file:  
```
MODULES="hid-logitech-dj"
```

Then run:
```
sudo mkinitcpio -P
```

And reboot.  

Ref: https://forum.manjaro.org/t/solaar-does-not-detect-logitech-mouse/80397
