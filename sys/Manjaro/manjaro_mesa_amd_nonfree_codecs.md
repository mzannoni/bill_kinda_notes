# Manjaro - non-free codecs

Manjaro doesn't support non-free codecs for AMD drivers.

There's a repo for them: https://nonfree.eu/

# Install
In short:  
```
❯ sudo pacman-key --recv-keys B728DB23B92CB01B && sudo pacman-key --lsign-key B728DB23B92CB01B
❯ sudo sh -c "curl -s https://nonfree.eu/stable/ > /etc/pacman.d/mesa-nonfree.pre.repo.conf"
❯ sudo sed -i '/^\[core\]/i \Include = /etc/pacman.d/mesa-nonfree.pre.repo.conf\n' /etc/pacman.conf
```


Note: the `stable` in the the second command line comes from  
```
❯ pacman-mirrors -G
```

# Uninstall

To remove the repo include from pacman.conf and reinstall the stock packages:  
```
❯ PKGS="$(pacman -Sl mesa-nonfree | grep 'installed' | awk '{print $2}')" && sudo sed -i '/mesa-nonfree/d' /etc/pacman.conf && sudo pacman -Sy $(echo ${PKGS//$'\n'/ })
```
