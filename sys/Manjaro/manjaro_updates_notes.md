# Manjaro update commands

## Main commands to update/upgrade

To do a full system update, using the command line interface of the package manager:  
```
pamac upgrade
```

Note: `pamac` should not be run with `sudo`, it asks for permission when needed.  

It is not clear if there is difference between `update` and `upgrade`  
```
pamac update
```

To use `pacman` instead:  
```
sudo pacman -Syu
```

With a double `y` option, a full database sync is enforced:  
```
sudo pacman -Syyu
```

## Useful commands to try if there are issues

If one repository fails to be updated, this can be tried:  
```
pamac upgrade --force-refresh
```

```
pamac update --force-refresh
```

This one should upgrade also packages from the AUR repo:  
```
pamac upgrade -a
```
 This again is the upgrade using `pacman`, that sometimes help solving issues:  
```
sudo pacman -Syyu
```

If nothing of the above works, reinstalling `ca-certificates` may help:  
```
sudo pacman -S ca-certificates
```

```
sudo pacman -Syy ca-certificates
```

### Packages from AUR

It can happen that due to some left over in the build temporary folder, it is impossible to update or re-install packages (e.g. if an error says something related to files not being valid or existent).

To clean the build cache:  
```zsh
pamac clean --build-files
```
and then try again.

### Allow downgrading packages

Someimes errors appear after upgrade, due to some wrong versioning (e.g. on package that is updated but then the current version is sort of receded). Pay always attention to the warnings from `pamac` or `pacman`.  
Example reference: https://forum.manjaro.org/t/after-upgrade-right-after-boot-i-get-a-blank-screen-and-no-login-page/157040  

In this case, it is possible to allow `pamac` to downgrade packages while updating the system:  
```
pamac upgrade --force-refresh --enable-downgrade
```

## Check dependencies

To check the dependency tree of a package (**what a package depends on**):  
```
pactree <package_name>
```

To check the **reverse** dependency tree (**what depends on a package**):  
```
pactree -r <package_name>
```

Note: the command `pactree` is part of the package `pacman-contrib`.  
