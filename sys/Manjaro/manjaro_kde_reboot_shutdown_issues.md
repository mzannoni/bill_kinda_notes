# KDE: reboot and shutdown doesn't work

## Symptoms

* When you reboot or shutdown from the KDE UI, the screen becomes black, but the mouse cursor is still showing and the machine doesn't reboot/shutdown.

* If you try rebooting or shutting down from a terminal (so, not from KDE UI), it works fine.

## Workaround

When it happens and the UI is hanging in there doing nothing:  
You can still get to a TTY (e.g. Ctrl+Alt+F3) and rebooot/shutdown from there.  
Example commands:
  ```zsh
  shutdown now
  systemctl poweroff
  sudo reboot
  systemctl reboot
  ```

## Fix

This is an issue with KDE session saving feature.  
* Go to `System Settings > Session > Desktop Session`
* Select `Start with an empty session` (instead of `When session was manually saved`)
* Reboot

There is a chance that enabling session saving again works fine, but not granted.  
Selecting the other option (`On last logout`) after the fix should work correctly.

## references
* [KDE bug](https://bbs.archlinux.org/viewtopic.php?id=297684)
* [Manjaro forum topic](https://forum.manjaro.org/t/kde-ui-shutdown-broken-but-not-the-command-line/168825)