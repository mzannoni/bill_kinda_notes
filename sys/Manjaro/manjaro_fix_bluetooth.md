# Desription
It can happen that the devices connected to Bluetooth don't behave correctly.  
Example of symptoms:  
* headphones stutters, the sounds break-up, no smooth listening (similarly for microphone)
* mouse is the very opposite of smooth, it jumps around, it's impossible to be precise

# Possible solution
Especially on laptop, it is possible that the autosuspend features applied to USB Bluetooth controllers is not behaving correctly (the BT device is often connected to the USB bus).

The following is a way that could fix it:

## 1. Create the rule to disable autosuspend
Create a file `40-custom.conf` in `/etc/tlp.d/` with inside:  
```
USB_EXCLUDE_BTUSB=1
```
and then run:  
```
sudo tlp usb
```
This should disable power saving on BT devices.

## 2. Check the status and eventually add kernel parameters
Run this command:  
```
❯ zcat /proc/config.gz | grep "CONFIG_BT_HCIBTUSB_AUTOSUSPEND"
```
if the output is `y`:  
```
CONFIG_BT_HCIBTUSB_AUTOSUSPEND=y
```
then the kernel parameter `btusb.enable_autosuspend=0` needs to be added: follow the procedure described in [kernel_paramenters.md](kernel_parameters.md).

## reference
These 2 posts in Manjaro forum:  
* https://forum.manjaro.org/t/usb-wireless-mouse-stutters/162722/12?u=billznn
* https://forum.manjaro.org/t/usb-wireless-mouse-stutters/162722/15?u=billznn
