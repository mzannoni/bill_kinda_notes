## Manjaro: restore KDE default theme and appearance

1. Log out of the GUI
2. switch to a `tty` with `Ctrl+Alt+F3`
3. Log in as the user whose graphic is to be reset
4. Execute:
    ```
    find $HOME/.config/ -maxdepth 1 -name 'plasma*' -exec rm {} \;
    rm -f $HOME/.cache/*.kcache
    sync
    ```
5. Log out of the `tty` with `Ctrl+D`
6. Switch back to the GUI with `Alt+F2`
7. Login to the GUI
