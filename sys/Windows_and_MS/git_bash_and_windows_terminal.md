# Add Git Bash to Windows Terminal

* Create an empty profile in MS Windows Terminal (mostly to generate a "guid").  
* Then, open the JSON file of the Windows Terminal settings (last button on the bottom left of the settings), and edit the newly create profile; in my case it was:  
  ```
  {
      "commandline": "%PROGRAMFILES%/Git/usr/bin/bash.exe -i -l",
      "guid": "{xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx}",
      "icon": "%PROGRAMFILES%/Git/mingw64/share/git/git-for-windows.ico",
      "hidden": false,
      "name": "Git Bash",
      "startingDirectory": "%USERPROFILE%"
  }
  ```
  Of course, guid, executable and icon locations, etc. will be different, depending on the PC.  

Ref: https://stackoverflow.com/questions/56839307/adding-git-bash-to-the-new-windows-terminal
