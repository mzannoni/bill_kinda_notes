To fix the stupid pen behavior in Windows, which, among other things, makes OneNote unusable with the pen:  

Control Panel -> Hardware and Sound -> Pen and Touch  

Then disable the action associated to touch & hold.  
In this way, the right click menu is never opened when your drawing slowly.  
