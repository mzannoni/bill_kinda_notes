# Win10 install on clean machine

## attempted
**!!! doesn't solve it !!!**

When you try to install Win10 on the clean machine, it will complain that it doesn't find a driver to install to.

The work around has been found here:
https://forums.tomshardware.com/threads/windows-cant-detect-hard-drive-on-clean-install.1609339/

Note: skip the "clean all" step

So, in summary, when you're in the installation process, press "shift+F10" to open the cmd.

Then, open the partitioning tool:  
```
>diskpart
```
List disks:
```
>list disk
```
Note the number of the disk you want to install Windows to (in our example, 1), then:
```
>select disk 1
```
And proceed to creation (the "format" step may take a while):
```
>create partition primary
>list partition
```
There should be only one...
```
>select partition 1
>active
>format fs=ntfs
>assign
>exit
>exit
```

**!!! doesn't solve it !!!**

Another try:
Downloaded Samsung SSD drivers and put on a USB stick, then tryed to have them seen by the installation tool
https://www.samsung.com/semiconductor/minisite/ssd/download/tools/


## SOLUTION:
Create the USB stick with Media Creation Tool for Windows
