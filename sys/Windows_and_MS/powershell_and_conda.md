# Unlock PowerShell to load conda script at start

After running `conda init powershell` to integrate `conda` in the PowerShell, the command `activate` doesn't really work, since scripts are blocked by default.  
You need to change execution policy and unlock the PowerShell profile script.  

```
Windows PowerShell

Copyright (C) Microsoft Corporation. All rights reserved.

Try the new cross-platform PowerShell https://aka.ms/pscore6

. : File C:\Users\sidbi\OneDrive\Documents\WindowsPowerShell\profile.ps1 cannot be loaded. The file
C:\Users\sidbi\OneDrive\Documents\WindowsPowerShell\profile.ps1 is not digitally signed. You cannot run this
script on the current system. For more information about running scripts and setting execution policy, see
about_Execution_Policies at https:/go.microsoft.com/fwlink/?LinkID=135170.
At line:1 char:3
+ . 'C:\Users\sidbi\OneDrive\Documents\WindowsPowerShell\prof ...
+   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    + CategoryInfo          : SecurityError: (:) [], PSSecurityException
    + FullyQualifiedErrorId : UnauthorizedAccess
Loading personal and system profiles took 501ms.
```

In the Powershell prompt: 
```
PS C:\Users\sidbi> Set-ExecutionPolicy -Scope CurrentUser -ExecutionPolicy RemoteSigned
PS C:\Users\sidbi> Unblock-File 'C:\Users\sidbi\OneDrive\Documents\WindowsPowerShell\profile.ps1'
```

Ref about execution policy:  
https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_execution_policies?view=powershell-7.2
