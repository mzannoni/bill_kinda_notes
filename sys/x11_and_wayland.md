## check if Wayland or X11 is running

sometimes, checking the variable XDG_SESSION_TYPE is enough:
```
$ echo $XDG_SESSION_TYPE
```

otherwise, get info on the current session with loginctl:
```
$ loginctl show-session "$XDG_SESSION_ID" -p Type
```

for all the info:
```
$ loginctl show-session "$XDG_SESSION_ID"
```
