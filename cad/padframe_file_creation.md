## To create a .dxf file for padframe:

- draw in Cadence layout editor shapes on top of the DV opening and of the CHIPEDGE (I used layers "`ALPHA drw`" and "`ALPHA pad`")
- export gds (short way: '`File --> Export Stream from VM`')
- open the gds with "`LayoutEditor`" (http://www.layouteditor.net/) and save it as `.dxf` (the same should be also possible with Glade - https://peardrop.co.uk/)
    - if needed, some basic editing can be done here (e.g. add labels, move origin)

- optional, e.g. if editing has to be finished: open the dxf in qcad and finalize (e.g. add/edit label, move origin)

---
