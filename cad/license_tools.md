# FlexLM license manager

## Check licenses
To check how many licenses:
```
lmstat -i -c  5280@serv1
```

To check checked-out:
```
lmstat -a -c ...
```

`lmstat` can be found at: `<IC615_install_folder>/tools/bin/lmstat`

---

## Command to shutdown `lmgrd` servers
```
/<IC615_install_folder>/tools/bin/lmdown -c /opt3/licenseFiles/License_xxxxx_xxxxxxxxxxxx_d_mm_yy.txt
```
or
```
<IC615_install_folder>/tools/bin/lmdown
```
and it makes you select which to terminate

---
