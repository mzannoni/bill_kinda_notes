# Virtuoso tips and tricks

## Kill currently exuting command

> `Ctrl+C`

e.g. if you inadvertently click a supply with the `Mark Net` tool in a layout window: after the `<Ctrl+C>` is given, the interruption of the command is not immediate, but will give you Cadence back in a reasonable time.  

## General Virtuoso Tips
from http://www.doe.carleton.ca/~tpepler/cadenceVirtuosoTips.html

### Lock Files

Cadence Virtuoso uses special files called lock files to designate designs that are currently open for edit. This is used to prevent a design from being edited from multiple users simultaneously.  

Unfortunately, if Virtuoso or your Linux session crashes, the lock files will still exist in your library directory and when you try to open the designs for edit, you will get an error. To fix this problem you have to delete the lock files, thankfully there is a simple tool provided by Cadence to manage this (I will also show how to achieve it with the linux find command).

#### Delete Lock Files Using the clsAdminTool Command

- Start Virtuoso (if not already running), and then
- open the Library Manager
- In the Library Manager, do '`File > Open Shell Window...`'. A new shell terminal should open up. N.B.: we open the shell this way so that all of the environment variables to point to the Virtuoso executables are already set.
- In the new shell that was opened in the last step, type  
  ```
  clsAdminTool
  ```
  You should now see the `clsAdminTool` prompt:
  ```
  >
  ```

- To list the lock files, at the prompt, enter
  ```
  ale .
  ```
  (do not forget the dot!)
- To remove the lock files, enter
  ```
  are .
  ```
  (again, the dot...)
- Then enter `quit` to quit the `clsAdminTool`, and `exit` to exit and close the shell window.

#### Delete Lock Files Using the Linux find Command (Less Preferred)

A word of warning, the `find` command (more specifically, the actions it can perform) is very powerful, and like the old saying goes, _"With great power comes great responsibility"_. It is pretty easy to permanently wipe out an entire directory with a badly formed find (and execute) command.  

- Follow the first few steps in the previous section to open a terminal from the Library Manager. Otherwise, in any terminal, navigate to the directory where your library is stored (usually whichever directory you launch Virtuoso from).
- To find and list all of the lock files, enter
  ```
  find . -name '*.cdslck*' -print
  ```
- To delete the files one at a time, prompting you before each deletion enter:
  ```
  find . -name '*.cdslck*' -exec rm -i {} \;
  ```
  Otherwise, to delete all of the lock files enter:
  ```
  find . -name '*.cdslck*' -exec rm {} \;
  ```
- When finished, enter `exit` to exit and close the shell window.


### Simulation Tips

#### Delta Markers

You can easily add delta markers (markers that show the time or voltage difference between 2 points, possibly between different waveforms), by hovering the cursor over the first point and type '`a`', then hover at the next point and type '`b`'.  

Alternatively, you can place 2 markers with the usual '`m`' key while hovering, then hold the `Ctrl` key while clicking both markers to select both simultaneously, then type '`Shift-d`'.

#### Smith Chart Markers

In a smith chart marker, edit the text and place one of these special codes to get the indicated values inserted at that point in the text.

* `%C` - Displays the real and imaginary cartesian values
* `%Z` - Displays the impedance values, such as resistance and reactance
* `%A` - Displays the admittance values, such as conductance and susceptance
* `%R` - Displays the reflection coefficients, such as mag and angle
* `%P` - Displays the polar values, such as mag and angle
* `%F` - Displays the frequency value, which includes the independent axis data

### Changing ADE Default Load / Save Option to “Cellview”

Default is “`Directory`” but saving states as cellviews is the preferred method (keeps everything organized by having your test bench schematic and simulation setup together); even Cadence recommends storing ADE states as cellviews and that the directory option is just for backward compatibility.

To make “`cellview`” the default option when you go to load or save a state in ADE L, in the directory in which you run Virtuoso edit (or create) the `.cdsinit` file and somewhere (usually put all of your customizations at the end) add these lines (the first is just a comment reminding you what the next line does):
```
; make ADE L save / load state option default to cellview 
envSetVal("asimenv" "saveAsCellview" 'boolean t)
```


### Layout Tips

#### Layout Selection Options

`Options > Selection...`: Uncheck “`Select objects larger than the window`” e.g. If you are zoomed in on your layout, you cannot accidentally select the image bevel.  

Make sure selection mode is “Full” if you are having trouble selecting objects to edit their properties or delete them.  

#### Layout Object Dimming

Dim other objects while selecting or moving a layout object: in layout window go to `Display > Options...`, then in the window bottom right set the `Dimming` drop down to "`other`"  
