# Useful file operation commands (for EDA tools)


to delete all the layout cell in a lib:
```
  $ find . -type d -name layout -exec rm -rf {} \;
```

make only files unexecutable in the current folder:
```
  $ find . -type f -exec chmod a-x  {} \;
```

delete all lock files in libraries (recursive through folders):
```
  $ find libraries/ -type f -name "*cdslck*" -exec rm -rf {} \;
```
everything after '`-exec`' is part of the command being executed, up to '`;`' that actually has to be escaped

---
