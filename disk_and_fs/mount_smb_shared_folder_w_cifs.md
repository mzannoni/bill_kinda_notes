# Mount shared folders using samba/cifs

This is to mount a remote folder (e.g. from a Sinology NAS) in Linux (specifically, in Ubuntu), using samba/cifs.  

First, make sure `cifs-utils` is installed (in Ubuntu, in other distros the package may have a different name).  

Essentially, two parts are needed:  
- line added in fstab
- text file storing the credentials for the access

## Step 0: make sure the destination folder exist and it's empty {#mnt-pnt}

Create a destination folder (mount point):  
```
$ mkdir <mount_folder>
```
where of course `<mount_folder>` is the path to a folder the user that mounts the remote folder has access to.

## Credential file {#cred-file}

If you don't want to mount manually all the time, and/or you don't want to be prompted for a password every time you mount a shared folder, one solution is to store the access credetials (username and password) for the shared resource (e.g. user of the Synology NAS).  

Makes sure this file is well hidden, best if in an encrypted partition and is decrypted only at login. This file will store username __and password__ in plain text!!!  

Let's say this file path is `<cifs_credentials>`, then inside there should be these 2 lines:  
```
username=usr_x
password=pwd_of_usr_x
```

## fstab setup

For each shared folder you need to mount, add a line to `/etc/fstab`

```
//<server_address>/<remote_folder>    <mount_folder>    credentials=<cifs_credentials>,uid=bill,gid=bill    0   0
```

Where:  
- `<server_address>` is the address of the remote server (e.g. the NAS)
- `<remote_folder>` is the name of the shared folder
- `<mount_folder>` is the mount point (created [above](#mnt-pnt), or anyhow available)
- `<cifs_credentials>` is the path to the credential file (described [just above](#cred-file))
- `uid` and `gid` specify which local user will be the owner of the mounted folder (_"mapped"_ to the remote user on the server)

## Additional notes

It is not always happening that all the folders in `fstab` are mounted at boot/login.  
It should be enough, after login, to run `mount -a` as root (e.g. using `sudo`) to mount all folders specified in `fstab` (and not only, actually...).  

Moreover, if an entry is present in `fstab`, that folder can be mounted manually simply with:  
```
$ sudo mount <mount_folder>
```
or  
```
$ sudo mount //<server_address>/<remote_folder>
```

## Manual mount

To mount a remote smb folder manually, use the `mount` command, specifying `cifs` as filesystem type:  
```
# mount -t cifs -o username=user_name //server_name/share_name <mount_folder>
```

A prompt will appear asking for the password.  
The `-o` option can be used to specify also other mounting options.
