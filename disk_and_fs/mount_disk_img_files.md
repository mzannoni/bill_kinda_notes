## Mount disk image files

To mount a disk image, e.g. made with `dd`  

```
$ sudo mount -o loop disk.img /mnt/bill/tmp
```

Of course, to unmout:
```
$  sudo umount /mnt/bill/tmp
```

It works better if the img file is local (not mounted through `cifs`, for example).  

If the img contains multiple partitions and/or a partition table, it may be that you need to specify the offset at which the partition you want to mount start:  

```
$ sudo mount -o loop,offset=<byte_count> disk.img /mnt/bill/tmp
```

The offset must be given in bytes.  
To get it, you can use `fdisk`:  

```
$ sudo fdisk -lu disk.img
```

And look for the "Begin" or "Start" column, but `fdisk` shows it in number of blocks, so you need to multiply this value by the block size (usually 512)
