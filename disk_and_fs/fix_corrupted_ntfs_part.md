# Fix a _broken_ NTFS partition

It can happen, e..g when youo incorrectly unplug an external disk, that the NTFS partition gets corrupted and is not mounted anymore.  

Try this to fix:  

```zsh
❯ ntfsfix /dev/sda1
❯ sudo ntfsfix -b /dev/sda1
❯ sudo ntfsfix -d /dev/sda1
```
Especially the last 2 commands did the trick (`-b` clears bad sectors and `-d` clears the _dirty flag_).  

## Forced partition unmount

This may be necessary form time to time if the resources is kept busy by an unreachable process.  

```zsh
❯ sudo umount -l <partition>
```
Where `<partition` is the busy partition to unmount (e.g. `/dev/sda1`).  

If it's a network location:  
```zsh
❯ sudo umount -f <location>
```

This operation can lead to loss of data, especially on partially unwritten files.  
After doing this, it is likely there's the need to fix the partition, as described [at the beginning](#fix-a-broken-ntfs-partition).  
