## Mount `.vdi` disk file
Typically, these are virtual machine disk files.  

`qemu` is required.  

```
$ sudo modprobe nbd
$ sudo qemu-nbd -c /dev/nbd0 <vdi_file>
$ sudo fdisk -l /dev/nbd0
```
then look what's the partition name  
```
$ sudo mount /dev/nbd0p1 /mnt
```

then, when done  
```
$ sudo umount /mnt
$ sudo qemu-nbd -d /dev/nbd0
```
