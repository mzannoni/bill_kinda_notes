# Methods to wipe a disk

- with `wipe`:
  ```
  $ wipe -r -q /path/to/wipe
  ```

- with `shred`:
  ```
  # shred -v /dev/sdX
  ```
  or
  ```
  # shred --verbose --random-source=/dev/urandom -n1 /dev/sdX
  ```

- with `dd`:
  ```
  # dd if=/dev/urandom of=/dev/sdX bs=4096 status=progress
  ```

---

links:  
- https://wiki.archlinux.org/index.php/Securely_wipe_disk
