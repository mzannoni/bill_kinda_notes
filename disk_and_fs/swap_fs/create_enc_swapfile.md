## To create and use an encrypted swapfile, stored in the SSD disk

Create the contiguous file:  
```
$ fallocate -l 4G /media/bill/SPEEDY/swap/cryptswap2
```

Add the following line to `/etc/crypttab`:  
```
cryptswap2 /media/bill/SPEEDY/swap/cryptswap2 /dev/urandom swap
```

Activate the encrypted drive:  
```
$ sudo service cryptdisks reload
```

Add the following line to `/etc/fstab`:  
```
/dev/mapper/cryptswap2 none swap sw 0 0
```

Start the swap disk:  
```
$ sudo cryptdisks_start cryptswap2
```

Activate all the swap:  
```
$ sudo swapon -a
```

To verify:  
```
$ swapon -s
$ free -h
```
