## To remove an encrypted swapfile

Look at the current swap files/partitions:  
```
❯ swapon --show
NAME      TYPE       SIZE USED PRIO
/dev/dm-0 partition 22.4G 2.3M   -2
/dev/dm-1 partition   16G   0B   -3

```

Try to guess which one is the one to disable (e.g. by size) and disable it:  
```
❯ sudo swapoff /dev/dm-1
```

I've tried to turn it off using the file path of the swapfile but it gave an error:
```
❯ sudo swapoff /media/bill/SPEEDY/swap/cryptswap2
swapoff: /media/bill/SPEEDY/swap/cryptswap2: swapoff failed: Invalid argument
```

Remove the entries from `/etc/fstab` and `/etc/crypttab`.  

Remote the swapfile:  
```
❯ sudo rm cryptswap2
```
