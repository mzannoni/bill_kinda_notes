## Increase the size of a swapfile

From Ubuntu 18.04, you can use a swapfile instead of a swap partition.  

If a swapfile already exists:  
```
$ sudo swapoff /swapfile
$ sudo rm  /swapfile
```

Then create a new swapfile:  
```
$ sudo dd if=/dev/zero of=/swapfile bs=1G count=4
```

Make it accessible for root only:  
```
$ sudo chmod 600 /swapfile
```

Then activate it:  
```
$ sudo mkswap /swapfile
$ sudo swapon /swapfile
```

To have it from startup, add a line like this to fstab:  
```
/swapfile       none        swap        sw      0   0
```

To list swap devices and files:  
```
$ swapon --show
```
