```
# ddrescue -r3 -n /dev/<baddrive> rescue.img rescue.log
# foremost -t all -i rescue.img -o <directory>
```

or
```
# ddrescue -r3 -n -f /dev/<baddrive> /dev/<gooddrive> rescue.log
# fdisk /dev/sdb
# fsck -v -f /dev/<gooddrive>1
# fsck -v -f /dev/<gooddrive>2
...
```
