- dd  
  ```
  $ dd if=/dev/mmcblk0 of=sd.img status=progress
  ```

- ddrescue (inside the package `gddrescue`)  
  ```
  $ ddrescue [OPTIONS] INFILE OUTFILE [LOGFILE]
  ```
  e.g.:
  ```
  $ sudo ddrescue -r 3 /dev/sda /media/usbdrive/image /media/usbdrive/logfile
  ```
  - passi successivi:  
    ```
    $ sudo ddrescue -r 3 -C /dev/sda /media/usbdrive/image /media/usbdrive/logfile
    ```

- foremost  
  ```
  $ sudo foremost -i image -o /recovery/foremost
  ```

- To try and restore lost partition table:  
  `testdisk`

---

refs:
- https://help.ubuntu.com/community/DataRecovery
