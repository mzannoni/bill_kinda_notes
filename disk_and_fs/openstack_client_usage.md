# OpenStack Python client

## Install

From `pip` or `conda`, install `python-openstackclient`.

Ref: https://github.com/openstack/python-openstackclient

## Authenticate

Instead of providing a long list of command line arguments, env vars can be set. If the password is not provided, it will be asked interactively.  

```
export OS_AUTH_URL=<url-to-openstack-identity>
export OS_IDENTITY_API_VERSION=3
export OS_PROJECT_NAME=<project-name>
export OS_PROJECT_DOMAIN_NAME=<project-domain-name>
export OS_USERNAME=<username>
export OS_USER_DOMAIN_NAME=<user-domain-name>
```

## subcommands

List the containers:  
`openstack container list`

Show details of a container:
`openstack container show <container>`

List objects in `<container>`  
`openstack object list <container>`  
with all details:  
`openstack object list <container> --long`  

Show details of one object:  
`openstack object show <container>`
