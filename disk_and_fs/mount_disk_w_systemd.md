# Setup systemd to mount disks

This is useful, for instance, to automount disks at startup on Manjaro.
It is equivalent to set them properly in the `/etc/fstab` (actually, the `fstab` at boot is *translated* to `systemd` services).

## Mount unit template

```
[Unit]
Description=

[Mount]
What=
Where=
#Type=
#Options=
#TimeoutSec=seconds

[Install]
WantedBy=multi-user.target
```

More templates can be found here: https://forum.manjaro.org/t/root-tip-how-to-systemd-mount-unit-samples/1191.

## Mount point

Tips:
 * don't use `/home` (it may not mount at all)
 * don't use `/mnt`, it's moslty intended for temporary mounts
 * don't use `/run`, it's *volatile* in its structure

One solution it to make a folder `/data` and add the mountpoint there.

## Unit file for a disk

Create a unit file for each disk to mount in `/etc/systemd/system/`, and call the unit file something very specific `path-to-mountpoint.mount`.
To understand, with an example: if the mount point (the `Where` in the unit file) is `/data/SPEEDY` then the unit name **must** be data-SPEEDY.mount.
The dash is sort of the folder separator of the path.

Also the extension `.mount` is important.

It is receommended to identify the disk by the `UUID` (note: **not** the partition UUID).
You can get this from a partition manager, or with the command:
```
❯ lsblk -no UUID /dev/sda1
```
### Fill in the unit file template

```
[Unit]
Description=

[Mount]
What=
Where=
#Type=
#Options=
#TimeoutSec=seconds

[Install]
WantedBy=multi-user.target
```
#### What

Disk to be mounted.
To identify it by UUID, use the path `/dev/disk/by-uuid/<UUID>`.

#### Where
Mount point location.
For instance a folder created in `/data`.

#### Options

Optional for disks, but for network devices, like NFS and SMB, this filed is required.
For instance, for a SMB share this could be `defaults,credentials=<file>,uid=<user>,gid=<group>`.

#### TimeoutSec

If not specified, it waits 90 seconds.

## Register and start the service

After the unit file is created, reload all units.
```
❯ sudo systemctl daemon-reload
```
And then start it:
```
❯ sudo systemctl start data-SPEEDY.mount
```

## Unit file for a SMB share

Make sure to have credentials stored in a text file accessible, for instance in the home. The file is something like:
```
username=<usr>
password=<pwd>
```
where of course user and password need to be specified.

The `systemd` unit needs to be like this:
```
[Unit]
Description=NAS folder
[Mount]
What=//<YOUR_SERVER>/<YOUR_SHARE>
Where=<YOUR_MOUNT_PATH>
Type=cifs
Options=_netdev,iocharset=utf8,rw,file_mode=0777,dir_mode=0777,credentials=<path/to/credential/file>,uid=<usr>,gid=<grp>,vers=<SMBVERSION>
TimeoutSec=30

[Install]
WantedBy=multi-user.target
```

`uid` and `gid` are needed if a user different from `root` needs to be the owner of the files.  
This is necessary, for instance, when the file metadata (e.g. modification time) must be edited by the user (e.g. when synchronizing with _FreeFileSync_).  

The option `vers` may not be needed, otherwise e.g. `NT1` can be tried.



## Mount at boot

For instance if the device is installed on the machine and always available, it is convenient to mount at boot.
```
❯ sudo systemctl enable data-SPEEDY.mount
```

## Automount

If the device is not always available, for instance a network drive or a USB drive, an `automount unit can be created`.

For the example above:
```
[Unit]
Description=Automount NAS folder
ConditionPathExists=<YOUR_MOUNT_PATH>

[Automount]
Where=<YOUR_MOUNT_PATH>
TimeoutIdleSec=10

[Install]
WantedBy=multi-user.target
```
Important: name the unit file according to the mount path (e.g. to mount `/data/NAS/Photos`, the unit file must be `data-NAS-Photos.automount`).

Then reload `systemctl` units and enable this unit.

The option `TimeoutIdleSec` controls after how many seconds of inactivity an unmount is attempted. Pass `0` to disable timeout. It's off by default.
