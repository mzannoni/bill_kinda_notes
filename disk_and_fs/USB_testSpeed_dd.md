## USB test speed

use `dd` to write 100 Mbyte of zeros (6250 blocks of 16000 bytes) to the USB drive
```
dd if=/dev/zero bs=16000 count=6250 of=/media/Ercole/testSpeed.img
```
