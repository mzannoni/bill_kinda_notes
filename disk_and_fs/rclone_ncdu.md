# rclone ncdu: tool to get storage statics on remotes, including Openstack containers

## `rclone` Install

With conda, install `rclone`

## `rclone` Usage

Run `rclone config` to setup a new remote.

The easiest way is to simply use the environment setup for the openstack client.  
So, in the `rclone` config file (usually in `~/.config/rclone`) just set the two vars:  
```
type = swift
env_auth = true
```
and leave everything else blank.  
The `env_auth` var set to true let `rclone` use the auth of openstack client (so, make sure that works first; ref: [openstack_client_usage.md](./openstack_client_usage.md)).

### Some commands for `rclone`

List the containers:  
```
rclone lsd remote:
```

## `rclone ncdu`

You can use a command of `rclone` which is the same as the one for local disks (`ncdu`).  

It is used like:  
```
rclone ncdu remote:
```

Once inside you can navigate folders etc.  
List of commands:  
```
 ↑,↓ or k,j to Move
 →,l to enter
 ←,h to return
 g toggle graph
 c toggle counts
 a toggle average size in directory
 m toggle modified time
 u toggle human-readable format
 n,s,C,A,M sort by name,size,count,asize,mtime
 d delete file/directory
 v select file/directory
 V enter visual select mode
 D delete selected files/directories
 y copy current path to clipboard
 Y display current path
 ^L refresh screen (fix screen corruption)
 ? to toggle help on and off
 q/ESC/^c to quit
```
