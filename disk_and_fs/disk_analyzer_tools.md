# Disk analysis tools for the command line

Especially useful when no graphics is available, e.g. in an SSH session.  

## `du` is the standard one

There are many tricks to use it effectively.  

* Summary of the current folder (human readable format):  
  ```
  du -chs .
  ```

* Summary of the content of the current folder (only first level, so files and folders in the current location):  
  ```
  du -chs ./*
  ```

* To look into more than the current level:  
  ```
  du -ch -d3 ./*
  ```
  where `-dN` sets the max depth to N (`-s` is equivalent to `-d0`).

* Then it is possible to sort the output, e.g. according to size with larger files or folders first:  
  ```
  du -chs ./* | sort  -hr
  ```
  this sorts the row according to human-readable numerical strings

## Other tools

### `ncdu`

Creates a more user friendly list.  

### `diskonaut`

Terminal based graphical navigation.  
