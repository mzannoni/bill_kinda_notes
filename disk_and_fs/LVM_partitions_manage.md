## To list LVM partitions/volumes:
```
# lvdisplay
```
or  
```
# lvscan
```
the latter does not give many details.


## Resize volumes

It is possible to "live" increase volumes, but to shrink a volume, it has to be unmounted.  

To increase volume to fill the available space:  
```
# lvextend /dev/mapper/fedora-root -l+100%FREE
```

Or to increase it by a certain amount (here +10Gbytes):  
```
# lvextend /dev/mapper/fedora-root -L+10G
```
Or to exactly 100Gbytes:  
```
# lvextend /dev/mapper/fedora-root -L10G
```

Then to increase the filesystem size, if you don't want to reboot:  
```
# resize2fs /dev/mapper/fedora-root
```

`resize2fs` can also be used to shrink a filesystem before shrinking an LVM volume.  

To make any logical volume available (not really needed):  
```
# vgchange -a y
```

So, to shrink an LVM volume:  

- check integrity  
  ```
  # e2fsck -f /dev/mapper/fedora-root
  ```

- resize filesystem:  
  ```
  # resize2fs /dev/polar/mapper/fedora-root 18G
  ```
  Here 18G is the new dimension. It has to be smaller than the desired new dimension of the volume, since a volume must not be shrinked to a dimension smaller then the filesystem in it; so, a practical and not risky way, instead of relying on exact calculation (tricky with logical dimensions) is to reduce the filesystem to a certain dimension, then the volume to a dimension slighlty bigger that the filesystem, then extending the filesystem again.  

- resize the volume:  
  ```
  # lvreduce -L 20G /dev/mapper/fedora-root
  ```
  Where 20G is the new dimensions.

- expand the filesystem again:  
  ```
  # resize2fs /dev/mapper/fedora-root
  ```
