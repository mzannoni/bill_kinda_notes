# Useful commands for disk management

## clone a disk:  
- unomunt the partitions first!
- then use dd  
  ```
  # dd if=/dev/sdX of=/dev/sdY bs=1M status=progress
  ```


for more details, especially if read errors occur:
https://wiki.archlinux.org/index.php/disk_cloning

---

### Burn bootable iso to USB with `dd`:
```
$ sudo dd bs=4M if=/path/to/file.iso of=/dev/sd<x> status=progress oflag=sync
```

The '`of`' field is the name of the device, not a partition (so, not ending with a number).  
**Important: make sure that the device letter you select is correct, it will be wiped!!!**  

To check it, you can use `fdisk`:
```
$ sudo fdisk -l
```
One typical way is to run `fdisk` before and after connecting the USB drive you want to use, so the correct device name can be found.

## Clone a partition

Like cloning a disk, but specify partitions instead, as input and output.  
Make sure the destination partition exist and to select the correct one.  
```
$ sudo dd if=/dev/sdb1 of=/dev/sdc1 status=progress
```
As usual, be very careful of the destination partition, it will be overwritten with no notice.  
The `status` option is of course optional.  
