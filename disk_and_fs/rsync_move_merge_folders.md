# Move and merge folders from command line using `rsync`

The command `mv` doesn't help here, because it can't be used to merge folder. If a folder in the destination already exist, it simply throws an error, without any further check nor action.   

To transfer and remove from the source the successfully transferred files (but folders are not deleted):  
```
rsync -av --remove-source-files source/ destination/
```

Then, to remove empty folders `find` can be used:  
```
find -depth -type d -empty -delete
``` 

After these two passages, what's left should be what was not successfully transferred by `rsync`.
