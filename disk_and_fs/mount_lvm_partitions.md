## Mount LVM partitions

Install lvm2
```
$ sudo apt install lvm2
```

Find info:
```
$ sudo vgscan
```
or
```
$ sudo vgscan --mknodes
```

Activate an LVM group:
```
$ sudo vgchange -ay
```
or
```
$ sudo vgchange -ay <group_name>
```

At this point you may already be able to mount it through Nautilus (under "Other Locations")  

To list the group:
```
$ lvdisplay
```
or
```
$ lvs
```

Get a good look with ls:
```
$ ls -l /dev/<group_name>
```

To mount the group, first create mount points, e.g.:
```
$ mkdir -vp /mnt/<group_name>/{root,home}
```

Then mount with:
```
$ sudo mount {LV_PATH} /path/to/mount/point/
```
e.g.:
```
$ sudo mount /dev/<group_name>/home /mnt/fedora/home
$ sudo mount /dev/<group_name>/root /mnt/fedora/root
```
